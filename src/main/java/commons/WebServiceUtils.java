package commons;

import java.io.IOException;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;

public class WebServiceUtils {

	public static String extractPostRequestBody(HttpServletRequest request) throws IOException {
	    if ("POST".equalsIgnoreCase(request.getMethod())) {
	        Scanner s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
	        return s.hasNext() ? s.next() : "";
	    }
	    return "";
	}
	
}
