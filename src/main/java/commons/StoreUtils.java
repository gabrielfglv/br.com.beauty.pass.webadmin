package commons;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import br.com.beauty.manager.StoreController;
import br.com.beauty.model.Category;
import br.com.beauty.model.Service;
import br.com.beauty.model.Store;

public class StoreUtils {
	
	public static Store getStoreByPost(HttpServletRequest request) {
		boolean error = false;
		        
		Store store = null;

        //Parametros do form
        String id = request.getParameter("id");
        String name = request.getParameter("title");
        String introduction = request.getParameter("introduction");
        String description = request.getParameter("description");
        String email = request.getParameter("email");
        String tel = request.getParameter("tel");
        String password = request.getParameter("password");
        String cnpj = request.getParameter("cnpj");
        String adress = request.getParameter("endereco");
        String cep = request.getParameter("cep");
        String coordXStr = request.getParameter("coordX");
        String coordYStr = request.getParameter("coordY");
        String categoryStr = request.getParameter("type");
        String priceLvlStr = request.getParameter("preco");

        String servicesToAddStr = request.getParameter("servicesToAdd");
        String servicesToEditStr = request.getParameter("servicesToEdit");

        float coordX = 0, coordY = 0;
        int category = -1, priceLvl = 0;
	    if (coordXStr != null && coordYStr != null && categoryStr != null) {
	        try {
		        coordX = Float.parseFloat(coordXStr.replaceAll(",", "."));
		        coordY = Float.parseFloat(coordYStr.replaceAll(",", "."));
		        category = Integer.parseInt(categoryStr);
		        priceLvl = Integer.parseInt(priceLvlStr);
	        }catch(NumberFormatException e) {
	        	error = true;
	        }
	    }else {
	    	error = true;
	    }
	    
	    if (name == null || name.isEmpty() || 
	    		introduction == null || introduction.isEmpty() || 
	    		email == null || email.isEmpty() || tel == null || 
	    		tel.isEmpty() || password == null || password.isEmpty() ||
	    		cnpj == null || cnpj.isEmpty() || 
	    		adress == null || adress.isEmpty() || 
	    		cep == null || cep.isEmpty() ) {
        	error = true;
	    }
        
	    if (!error) {
	    	
	    	store = new Store();
	    	store.setId(id);
	    	store.setTitle(name);
	    	store.setIntroduction(introduction);
	    	store.setDescription(description);
	    	store.setEmail(email);
	    	store.setTelefone(tel);
	    	store.setPassword(StringUtils.criptoString(password));
	    	store.setCnpj(cnpj);
	    	store.setAdress(adress);
	    	store.setCep(cep);
	    	store.setCoordX(coordX);
	    	store.setCoordY(coordY);
	    	store.setPriceLvl(priceLvl);
	    	store.setCategory(new Category(category));
	    	
	    }
	    
	    return store;
	}
	
	public static ArrayList<Service> getServicesByJSONEdit(String jsonStr){
		
		JSONParser jsonParse = new JSONParser();
		JSONObject jsonToEdit;
		try {
			jsonToEdit = (JSONObject) jsonParse.parse(jsonStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
		ArrayList<Service> listOfServices = null;
		Set<String> keys = jsonToEdit.keySet();
		
		if (keys.size() > 0) {
			listOfServices = new ArrayList<Service>();
			for (String key : keys) {
				
				try {
					JSONObject jsonObj = (JSONObject) jsonToEdit.get(key);
					if (jsonObj != null) {

						Service newService = getServiceByObject(jsonObj);
						newService.setId(key);
						
						listOfServices.add(newService);
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		return listOfServices;
	}
	
	public static ArrayList<Category> getCategoryByJSONEdit(String jsonStr){
		
		JSONParser jsonParse = new JSONParser();
		JSONObject jsonToEdit;
		try {
			jsonToEdit = (JSONObject) jsonParse.parse(jsonStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
		ArrayList<Category> listOfCategorys = null;
		Set<String> keys = jsonToEdit.keySet();
		
		if (keys.size() > 0) {
			listOfCategorys = new ArrayList<Category>();
			for (String key : keys) {
				
				try {
					String nameSegment = (String)jsonToEdit.get(key);
					if (nameSegment != null) {

						Category newCategory = new Category(key);
						newCategory.setName(nameSegment);
						
						listOfCategorys.add(newCategory);
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		return listOfCategorys;
	}
	
	public static ArrayList<String> getIdsByJSONDelete(String jsonStr){
		
		JSONParser jsonParse = new JSONParser();
		JSONArray jsonArrToDelete;
		try {
			jsonArrToDelete = (JSONArray) jsonParse.parse(jsonStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
		ArrayList<String> listOfIdServices = null;
		
		if (jsonArrToDelete.size() > 0) {
			listOfIdServices = new ArrayList<>();
			for (int index = 0; index < jsonArrToDelete.size(); index++) {
				listOfIdServices.add(jsonArrToDelete.get(index).toString());
			}
		}
		return listOfIdServices;
	}
	
	/*public static ArrayList<Service> getServicesByJSONAdd(String jsonStr){
		
		JSONParser jsonParse = new JSONParser();
		JSONArray jsonArrToAdd;
		try {
			jsonArrToAdd = (JSONArray) jsonParse.parse(jsonStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
		ArrayList<Service> listOfServices = null;
		
		if (jsonArrToAdd.size() > 0) {
			listOfServices = new ArrayList<Service>();
			for (int index = 0 ; index < jsonArrToAdd.size() ; index++) {
				
				try {
					JSONObject jsonObj = (JSONObject) jsonArrToAdd.get(index);
					if (jsonObj != null) {

						Service newService = getServiceByObject(jsonObj);
						listOfServices.add(newService);
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		return listOfServices;
	}*/
	
	private static Service getServiceByObject(JSONObject jsonObj) {
		
		Service newService = new Service();

		boolean valid = jsonObj.get("valid").toString().equals("true");
		boolean highlight = jsonObj.get("highlight").toString().equals("true");
		int position = Integer.parseInt(jsonObj.get("position").toString());
		
		newService.setValid(valid);
		newService.setHightlight(highlight);
		newService.setPosition(position);
		
		return newService;
	}
	
	public static ArrayList<String> getSegmentsToAdd(String jsonStr){

		JSONParser jsonParse = new JSONParser();
		JSONArray jsonArrToAdd;
		try {
			jsonArrToAdd = (JSONArray) jsonParse.parse(jsonStr);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
		ArrayList<String> listOfNames = null;
		
		if (jsonArrToAdd.size() > 0) {
			listOfNames = new ArrayList<>();
			for (int index = 0; index < jsonArrToAdd.size(); index++) {
				listOfNames.add(jsonArrToAdd.get(index).toString());
			}
		}
		return listOfNames;
	}

}
