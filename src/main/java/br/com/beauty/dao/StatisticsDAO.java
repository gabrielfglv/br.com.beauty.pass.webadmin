package br.com.beauty.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import br.com.beauty.factory.ConnectionFactory;
import br.com.beauty.model.Category;
import br.com.beauty.model.Service;
import br.com.beauty.model.ServiceStatistic;
import br.com.beauty.model.Statistic;
import br.com.beauty.model.Store;

public class StatisticsDAO  extends ConnectionFactory {


	private static StatisticsDAO instance;

	/**
	 * Metodo responsovel por criar uma instancia da classe.
	 */
	public static StatisticsDAO getInstance() {
		if (instance == null)
			instance = new StatisticsDAO();
		return instance;
	}

	
	public Statistic getStatisticStore(int storeId) {

		Statistic statistic = null;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT estabelecimento.nome, estabelecimento.nota, (SELECT COUNT(ID) FROM servico_agendado WHERE idLoja = ?) AS quant,\r\n" + 
								"        (SELECT COUNT(ID) FROM servico_agendado WHERE confirmado = -1 and idLoja = ?) AS notConfirmed,\r\n" + 
								"        (SELECT COUNT(ID) FROM servico_agendado WHERE confirmado = 0 and idLoja = ?) AS confirmed,\r\n" + 
								"        (SELECT COUNT(ID) FROM servico_agendado WHERE confirmado = 1 and idLoja = ?) AS declined,\r\n" + 
								"        (SELECT SUM(total) FROM servico_agendado WHERE idLoja = ?) AS value"+
								"        from estabelecimento WHERE estabelecimento.id = ?");
				pstmt.setInt(1, storeId);
				pstmt.setInt(2, storeId);
				pstmt.setInt(3, storeId);
				pstmt.setInt(4, storeId);
				pstmt.setInt(5, storeId);
				pstmt.setInt(6, storeId);
				rs = pstmt.executeQuery();
				
				statistic = new Statistic();
	
				if (rs.next()) {
					Store store = new Store();
					store.setId(storeId);
					store.setTitle(rs.getString("nome"));
					store.setScore(rs.getFloat("nota"));
					statistic.setStore(store);
					statistic.setQtAval(rs.getInt("quant"));
					statistic.setQtAcept(rs.getInt("confirmed"));
					statistic.setQtDeclined(rs.getInt("declined"));
					statistic.setQtNotConfirmed(rs.getInt("notConfirmed"));	
					statistic.setTotalValue(rs.getFloat("value"));
					
					HashMap<Integer, ServiceStatistic> hashServices = null;
					
					pstmt = conexao
							.prepareStatement("SELECT servico_loja.id, servico_loja.titulo, agenda.avaliacao, agendaQt.qt, valueService.valueFinal FROM servico_loja \r\n" + 
									"left join (select idServico, AVG(avaliacao) as avaliacao from servico_agendado WHERE avaliacao > 0 GROUP BY idServico) AS agenda ON servico_loja.id = agenda.idServico \r\n" + 
									"left join (select idServico, COUNT(id) as qt from servico_agendado GROUP BY idServico) AS agendaQt ON servico_loja.id = agendaQt.idServico \r\n" + 
									"left join (select idServico, SUM(total) as valueFinal from servico_agendado WHERE confirmado < 1 GROUP BY idServico) AS valueService ON servico_loja.id = valueService.idServico"
									+ " WHERE id_loja = ?");
					pstmt.setInt(1, storeId);
					rs = pstmt.executeQuery();
					
					ArrayList<ServiceStatistic> listServices = new ArrayList<>();
					while (rs.next()) {
					
						ServiceStatistic newService = new ServiceStatistic();
						newService.setId(rs.getInt("id"));
						newService.setTitle(rs.getString("titulo"));
						newService.setAvgScore(rs.getFloat("avaliacao"));
						newService.setQtAval(rs.getInt("qt"));
						newService.setTotalValue(rs.getFloat("valueFinal"));
						listServices.add(newService);
					
					}
					statistic.setListOfServices(listServices);
				}
	
			} catch (Exception e) {
				System.out.println("Erro ao obter as estastisticas da loja do id: " + storeId+ " - "+e);
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		
		return statistic;
	}
	
	
}
