package br.com.beauty.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import br.com.beauty.factory.ConnectionFactory;
import br.com.beauty.model.User;
import br.com.beauty.model.Store;

/**
 * Classe responsovel por conter os metodos do CRUD.
 */
public class UserDAO extends ConnectionFactory {

	private static UserDAO instance;

	/**
	 * Metodo responsovel por criar uma instancia da classe ClienteDAO.
	 */
	public static UserDAO getInstance() {
		if (instance == null)
			instance = new UserDAO();
		return instance;
	}

	/**
	 * @return ArrayList<Cliente>
	 * Metodo responsavel por buscar e listar todos os clientes gravados no  banco de dados.
	 */
	public ArrayList<User> listarTodos() {
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<User> clientes = null;

		conexao = criarConexao();
		clientes = new ArrayList<User>();
		try {
			pstmt = conexao
					.prepareStatement("SELECT * FROM cliente ORDER BY nome");
			rs = pstmt.executeQuery();

			while (rs.next()) {
				User cliente = new User();

				cliente.setId(rs.getInt("id"));
				cliente.setNome(rs.getString("nome"));
				cliente.setCpf(rs.getString("cpf"));
				cliente.setEndereco(rs.getString("endereco"));

				clientes.add(cliente);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos os clientes: " + e);
			e.printStackTrace();
		} finally {
			fecharConexao(conexao, pstmt, rs);
		}
		return clientes;
	}

	/**
	 * Busca um cliente no banco dado um id.
	 * 
	 * @param id
	 * @return cliente
	 */
	public User getById(long id) {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		User cliente = null;
		conexao = criarConexao();

		try {
			pstmt = conexao
					.prepareStatement("SELECT * FROM cliente WHERE id = ?");
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				cliente = new User();
				cliente.setId(rs.getInt("id"));
				cliente.setNome(rs.getString("nome"));
			}
		} catch (Exception e) {
			System.out
					.println("Erro ao buscar cliente com ID=" + id + "\n" + e);
			e.printStackTrace();
		} finally {
			fecharConexao(conexao, pstmt, rs);
		}

		return cliente;

	}

	/**
	 * Metodo responsavel por gravar cliente no banco de dados.
	 * 
	 * @param cliente
	 * @return verdade se cliente gravado e falso se nao gravado
	 */
	public User insert(String ip, String localeUser) {

		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();
		User cliente = null;
		try {
			pstmt = conexao
					.prepareStatement("insert into cliente(nome,cpf,criacao,ipCriacao,localCriacao)"
							+ "values('temp','31312321',now(),?,?)", Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1,ip);
			pstmt.setString(2, localeUser);
			int execute = pstmt.executeUpdate();
			cliente = new User();
			
			ResultSet resultSet = pstmt.getGeneratedKeys();
	        int newId = -1;
			if (resultSet.next()) {
				newId = resultSet.getInt(1);
				pstmt.execute("UPDATE cliente set nome = 'user"+newId+"' where id = "+newId);
				cliente.setId(newId);
				cliente.setNome("user"+newId);
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			cliente = null;
			e.printStackTrace();

		}
		return cliente;
	}

	/**
	 * Metodo responsavel por atualizar cliente na base de dados
	 * 
	 * @param cliente
	 * @return verdade se atualizado e falso se nao.
	 */
	public boolean update(User cliente) {
		long id = cliente.getId();
		String nome = cliente.getNome();
		String cpf = cliente.getCpf();
		String endereco = cliente.getEndereco();
		boolean isAtualizado = false;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();
		try {
			pstmt = conexao.prepareStatement("UPDATE cliente SET nome =?,cpf = ?,endereco = ? WHERE id = ?");
			pstmt.setString(1, nome);
			pstmt.setString(2, cpf);
			pstmt.setString(3, endereco);
			pstmt.setLong(4, id);
			int execute = pstmt.executeUpdate();
			isAtualizado = true;
			System.out.println("Retorno update: " + execute);

		} catch (SQLException e) {
			isAtualizado = false;
			e.printStackTrace();

		} finally {
			fecharConexao(conexao, pstmt, null);
		}
		return isAtualizado;

	}

	/**
	 * Metodo responsavel por deletar cliente na base de dados.
	 * 
	 * @param id
	 * @return Verdade se cliente deletado e falso se nao.
	 */
	public boolean delete(User cliente) {
		boolean isDeletado = false;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();
		try {
			pstmt = conexao.prepareStatement("DELETE FROM cliente WHERE id = ?");
			pstmt.setInt(1, cliente.getId());
			boolean execute = pstmt.execute();
			isDeletado = true;
			System.out.println("Respota do delete: " + execute);

		} catch (SQLException e) {
			isDeletado = false;
			e.printStackTrace();

		} finally {
			fecharConexao(conexao, pstmt, null);
		}
		return isDeletado;
	}

}
