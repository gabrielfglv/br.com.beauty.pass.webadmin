package br.com.beauty.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import br.com.beauty.factory.ConnectionFactory;
import br.com.beauty.model.Category;
import br.com.beauty.model.ScheduledService;
import br.com.beauty.model.Service;
import br.com.beauty.model.Store;
import br.com.beauty.model.User;

public class CalendarDAO extends ConnectionFactory {


	private static CalendarDAO instance;

	/**
	 * Metodo responsovel por criar uma instancia da classe.
	 */
	public static CalendarDAO getInstance() {
		if (instance == null)
			instance = new CalendarDAO();
		return instance;
	}

	public boolean isDateValid(long dateTime, int idService) throws Exception{
		
		boolean isOnLimit = false;
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT count(dateTime) as qt, servico_loja.limite  FROM servico_agendado inner join servico_loja "
								+ "on servico_agendado.idServico = servico_loja.id where servico_agendado.dateTime = ? AND servico_agendado.idServico = ? AND confirmado < 1");
				pstmt.setLong(1, dateTime);
				pstmt.setInt(2, idService);
				rs = pstmt.executeQuery();
				
				if(rs.next()) {
					int quantity = rs.getInt("qt");
					int limit = rs.getInt("limite");
					if (quantity >= limit) {
						isOnLimit = true;
					}
				}
	
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Erro ao consultar o banco de dados");
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return isOnLimit;
	}
	
	public HashMap<Long, Integer> getUsedDates(int idService){
		
		HashMap<Long, Integer> listOfDateTimes = null;
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT dateTime FROM servico_agendado where idServico = ? AND confirmado < 1");
				pstmt.setInt(1, idService);
				rs = pstmt.executeQuery();
				listOfDateTimes = new HashMap();
				
				while(rs.next()) {
					Long longDate = rs.getLong("dateTime");
					if (listOfDateTimes.containsKey(longDate)) {
						listOfDateTimes.put(longDate, listOfDateTimes.get(longDate)+1);
					}else {
						listOfDateTimes.put(longDate, 1);
					}
					
				}
	
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return listOfDateTimes;
	}
	
	public boolean insertScheduledService(ScheduledService scheduledService) {

		boolean allRight = true;
		PreparedStatement pstmt = null;
		Connection conexao = criarConexao();
		ResultSet rs = null;

			if (conexao != null) {
			try {
				
				pstmt = conexao
						.prepareStatement("SELECT servico_loja.titulo as 'nameService', estabelecimento.id as 'idStore' "+
								"FROM servico_loja INNER JOIN "+
								"estabelecimento ON servico_loja.id_loja = estabelecimento.id "+
								"WHERE servico_loja.id = ?");
				pstmt.setInt(1, scheduledService.getIdService());
				rs = pstmt.executeQuery();
				
				int idStore = -1;
				String nameService = null;
				
				if (rs.next()) {
					idStore = rs.getInt("idStore");
					nameService = rs.getString("nameService");
				}
				
				if (idStore > -1 && nameService != null){
					
					pstmt = conexao
							.prepareStatement("insert into servico_agendado(dateTime, dataAgendada, horaAgendada, "
									+ "idUser, idLoja, idServico, nomeServico, total, opcoes, dataAgendamento)"
									+ "values(?,?,?,?,?,?,?,?,?, now())");
					pstmt.setLong(1, scheduledService.getDateTime());
					pstmt.setDate(2, scheduledService.getDate());
					pstmt.setTime(3, scheduledService.getTime());
					pstmt.setInt(4, scheduledService.getIdUser());
					pstmt.setInt(5, idStore);
					pstmt.setInt(6, scheduledService.getIdService());
					pstmt.setString(7, nameService);
					pstmt.setDouble(8, scheduledService.getValue());
					pstmt.setString(9, scheduledService.getOptions());
					
					pstmt.execute();
					
				}else {
					allRight = false;
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				allRight = false;
				e.printStackTrace();
			}
		}
			
		return allRight;
	}
	
	public ArrayList<ScheduledService> getUsedDatesFromUser(int idUser){
		
		ArrayList<ScheduledService> listOfServices = null;
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM servico_agendado "
								+ "INNER JOIN estabelecimento ON estabelecimento.id = servico_agendado.idLoja "
								+ "where idUser = ? ORDER BY dateTime");
				pstmt.setInt(1, idUser);
				rs = pstmt.executeQuery();
				listOfServices = new ArrayList<>();
				
				while(rs.next()) {
					ScheduledService newScheduled = new ScheduledService();
					newScheduled.setId(rs.getInt("id"));
					newScheduled.setDateTime(rs.getLong("dateTime"));
					newScheduled.setConfirmed(rs.getInt("confirmado"));
					newScheduled.setNameService(rs.getString("nomeServico"));
					newScheduled.setIdService(rs.getInt("idServico"));
					newScheduled.setOptions(rs.getString("opcoes"));
					newScheduled.setValue(rs.getDouble("total"));
					newScheduled.setScore(rs.getInt("avaliacao"));
					
					Store newStore = new Store();
					newStore.setId(rs.getInt("idLoja"));
					newStore.setTitle(rs.getString("nome"));
					newStore.setTelefone(rs.getString("telefone"));
					newStore.setEmail(rs.getString("email"));
					newStore.setAdress(rs.getString("endereco"));
					newStore.setCep(rs.getString("cep"));
					newStore.setIdImage(rs.getInt("imageid"));
					
					newScheduled.setStore(newStore);
					
					listOfServices.add(newScheduled);
				}
	
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return listOfServices;
		
	}
	
	public ArrayList<ScheduledService> getUsedDatesFromStore(int idStore){
		
		ArrayList<ScheduledService> listOfServices = null;
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM servico_agendado "
								+ "INNER JOIN cliente ON cliente.id = servico_agendado.idUser "
								+ "where idLoja = ? ORDER BY dateTime");
				pstmt.setInt(1, idStore);
				rs = pstmt.executeQuery();
				listOfServices = new ArrayList<>();
				
				while(rs.next()) {
					ScheduledService newScheduled = new ScheduledService();
					newScheduled.setId(rs.getInt("id"));
					newScheduled.setDateTime(rs.getLong("dateTime"));
					newScheduled.setConfirmed(rs.getInt("confirmado"));
					newScheduled.setNameService(rs.getString("nomeServico"));
					newScheduled.setScore(rs.getInt("avaliacao"));
					newScheduled.setIdService(rs.getInt("idServico"));
					newScheduled.setOptions(rs.getString("opcoes"));
					newScheduled.setValue(rs.getDouble("total"));
					newScheduled.setIdStore(idStore);
					newScheduled.setDateTimeMarked(rs.getDate("dataAgendamento"));
					
					User newUser = new User();
					newUser.setId(rs.getInt("idUser"));
					newUser.setNome(rs.getString("nome"));
					newScheduled.setClient(newUser);
					
					listOfServices.add(newScheduled);
				}
	
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return listOfServices;
	}
	
	public ScheduledService getScheduledService(int id){
		
		ScheduledService newScheduled = null;
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				pstmt = conexao
						.prepareStatement("SELECT * FROM servico_agendado "
								+ "INNER JOIN cliente ON cliente.id = servico_agendado.idUser "
								+ "INNER JOIN estabelecimento ON estabelecimento.id = servico_agendado.idLoja "
								+ "where servico_agendado.id = ?");
				pstmt.setInt(1, id);
				rs = pstmt.executeQuery();
				
				if(rs.next()) {
					newScheduled = new ScheduledService();
					newScheduled.setId(rs.getInt("servico_agendado.id"));
					newScheduled.setDateTime(rs.getLong("dateTime"));
					newScheduled.setConfirmed(rs.getInt("confirmado"));
					newScheduled.setNameService(rs.getString("nomeServico"));
					newScheduled.setScore(rs.getInt("avaliacao"));
					newScheduled.setIdService(rs.getInt("idServico"));
					newScheduled.setOptions(rs.getString("opcoes"));
					newScheduled.setValue(rs.getDouble("total"));
					newScheduled.setIdStore(rs.getInt("idLoja"));
					newScheduled.setDateTimeMarked(rs.getDate("dataAgendamento"));
					
					User newUser = new User();
					newUser.setId(rs.getInt("idUser"));
					newUser.setNome(rs.getString("cliente.nome"));
					newScheduled.setClient(newUser);
					
					Store newStore = new Store();
					newStore.setId(rs.getInt("idLoja"));
					newStore.setTitle(rs.getString("estabelecimento.nome"));
					newScheduled.setStore(newStore);
					
				}
	
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return newScheduled;
	}
	
	public boolean updateScheduledServiceStatus(int id, int status) {
		boolean sucess = true;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				
			pstmt = conexao
						.prepareStatement("update servico_agendado set confirmado = ? where id = ?");
				pstmt.setInt(1, status);
				pstmt.setInt(2, id);
				boolean exec = pstmt.execute();
			
			} catch (Exception e) {
				System.out.println("Erro ao apagar os segmentos - "+e);
				sucess = false;
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		return sucess;
	}
	
	public boolean updateScoreService(int id, int value) {
		boolean sucess = true;
		
		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		conexao = criarConexao();
		
		if (conexao != null) {
			try {
				
			pstmt = conexao
						.prepareStatement("update servico_agendado set avaliacao = ? where id = ?");
				pstmt.setInt(1, value);
				pstmt.setInt(2, id);
				boolean exec = pstmt.execute();
			
			} catch (Exception e) {
				System.out.println("Erro ao apagar os segmentos - "+e);
				sucess = false;
				e.printStackTrace();
			} finally {
				fecharConexao(conexao, pstmt, rs);
			}
		}
		
		return sucess;
	}
	
	
	
}
