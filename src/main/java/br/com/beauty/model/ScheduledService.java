package br.com.beauty.model;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mortbay.util.ajax.JSON;

import commons.StringUtils;

public class ScheduledService {
	
	private int id = -1,
			idStore = -1,
			idService = -1,
			idUser = -1,
			score  = -1,
			confirmed = -1;
	private long dateTime = -1;
	private Date date;
	private Time time;
	private String nameService,
					options;
	private JSONArray jsonObjectOptions = null;
	private ArrayList<ServiceOption> listOfOptions;
	private double value = -1; 
	private Store store;
	private User client;
	private boolean executed;
	private Date dateTimeMarked;
	private Timestamp timeStamp;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdStore() {
		return idStore;
	}
	public void setIdStore(int idStore) {
		this.idStore = idStore;
	}
	public int getIdService() {
		return idService;
	}
	public void setIdService(int idService) {
		this.idService = idService;
	}
	public long getDateTime() {
		return dateTime;
	}
	public void setDateTime(long dateTime) {
		//Zera os segundos e nanoSegundos
		timeStamp = new Timestamp(dateTime);
		timeStamp.setSeconds(0);
		timeStamp.setNanos(0);
		
		this.dateTime = timeStamp.getTime();
		this.date = new Date(dateTime);
		this.time = new Time(dateTime);
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	public String getNameService() {
		return nameService;
	}
	public void setNameService(String nameService) {
		this.nameService = nameService;
	}
	public String getOptions() {
		return options;
	}
	
	/**
	 * Transforma uma string de options em JSONArray
	 * @param options
	 */
	public void setOptions(String options) {
		this.options = options;
		this.jsonObjectOptions = new JSONArray();
		JSONArray jsonArr = StringUtils.toJSONArr(options);
		if (jsonArr != null) {
			listOfOptions = new ArrayList<>();
			for (int indexArr = 0; indexArr < jsonArr.size(); indexArr++) {
				JSONObject jsonObj = StringUtils.toJSONObj(jsonArr.get(indexArr).toString());
				this.jsonObjectOptions.add(jsonObj);
				
				ServiceOption serviceOption = new ServiceOption();
				serviceOption.setId(Integer.parseInt(jsonObj.get("id").toString()));
				serviceOption.setTitle(jsonObj.get("title").toString());
				serviceOption.setValor(Double.parseDouble(jsonObj.get("value").toString()));
				listOfOptions.add(serviceOption);
			}
		}
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public int getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(int confirmed) {
		this.confirmed = confirmed;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	
	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	public JSONObject toJSONObject() {
		JSONObject jsonToReturn = new JSONObject();
		
		jsonToReturn.put("id", id);
		jsonToReturn.put("value", value);
		jsonToReturn.put("score", score);
		jsonToReturn.put("confirmed", confirmed);
		jsonToReturn.put("options", jsonObjectOptions);
		jsonToReturn.put("executed", executed);
		jsonToReturn.put("statusStr", getConfirmedString());
		
		JSONObject date = new JSONObject();
		date.put("day", StringUtils.getDateFormated(this.date));
		date.put("dateTime", timeStamp.getTime());
		date.put("time", StringUtils.getTimeFormated(time));
		date.put("dayOfWeek", StringUtils.getDayOfWeek(this.date.getDay()));
		date.put("completeDate", StringUtils.getCompleteDate(this.date));
		date.put("simplificateDate", StringUtils.getSimplificateDate(this.timeStamp));
		
		jsonToReturn.put("date", date);
		
		JSONObject service = new JSONObject();
		service.put("id", idService);
		service.put("name", nameService);
		
		jsonToReturn.put("service", service);
		
		JSONObject store = new JSONObject();
		store.put("id", this.store.getId());
		store.put("title", this.store.getTitle());
		store.put("tel", this.store.getTelefone());
		store.put("email", this.store.getEmail());
		store.put("adress", this.store.getAdress());
		store.put("cep", this.store.getCep());
		store.put("imageId", this.store.getIdImage());
		
		jsonToReturn.put("store", store);
		
		return jsonToReturn;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public boolean isExecuted() {
		return executed;
	}
	public void setExecuted(boolean executed) {
		this.executed = executed;
	}
	public User getClient() {
		return client;
	}
	public void setClient(User client) {
		this.client = client;
	}
	public Date getDateTimeMarked() {
		return dateTimeMarked;
	}
	public void setDateTimeMarked(Date dateTimeMarked) {
		this.dateTimeMarked = dateTimeMarked;
	}
	public String getFormatedDate() {
		return StringUtils.getDateFormated(date);
	}
	public String getFormatedTime() {
		return StringUtils.getTimeFormated(time);
	}
	
	public ArrayList<ServiceOption> getListOfOptions() {
		return listOfOptions;
	}
	public String getConfirmedString() {
		if (executed) {
			switch(confirmed) {
				case 1:
					return "Rejeitado";
				default:
					return "Executado";
			}
		}else {
			switch(confirmed) {
				case 0:
					return "Confirmado";
				case 1:
					return "Rejeitado";
				default:
					return "Não confirmado";
			}
		}
	}
}
