package br.com.beauty.model;

public class ServiceStatistic {

	private int id,
				qtAval,
				qtDeclined,
				qtAcept,
				qtNotConfirmed,
				qtAcess;
	private float avgScore,
				totalValue;
	private String title;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQtAval() {
		return qtAval;
	}
	public void setQtAval(int qtAval) {
		this.qtAval = qtAval;
	}
	public int getQtDeclined() {
		return qtDeclined;
	}
	public void setQtDeclined(int qtDeclined) {
		this.qtDeclined = qtDeclined;
	}
	public int getQtAcept() {
		return qtAcept;
	}
	public void setQtAcept(int qtAcept) {
		this.qtAcept = qtAcept;
	}
	public int getQtNotConfirmed() {
		return qtNotConfirmed;
	}
	public void setQtNotConfirmed(int qtNotConfirmed) {
		this.qtNotConfirmed = qtNotConfirmed;
	}
	public int getQtAcess() {
		return qtAcess;
	}
	public void setQtAcess(int qtAcess) {
		this.qtAcess = qtAcess;
	}
	public float getAvgScore() {
		return avgScore;
	}
	public void setAvgScore(float avgScore) {
		this.avgScore = avgScore;
	}
	public float getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(float totalValue) {
		this.totalValue = totalValue;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
