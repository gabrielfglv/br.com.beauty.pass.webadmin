package br.com.beauty.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import br.com.beauty.manager.PropertiesController;
import commons.CalcUtils;

public class Store {

	private int id = -1,
				priceLvl,
				countAvaliable,
				idImage;
	private String title,
				cnpj,
			    introduction,
			    description,
			    email,
			    telefone,
			    password,
			    adress,
			    complement,
			    cep;

	private ArrayList<Service> listOfServices = null;
	private ArrayList<Service> listOfServicesToAdd;
	private float coordX,
				coordY,
				score;
	private double distanceFromDestiny = -1;
	private boolean valid;
	private Date createDate;
	private Category category;
	
	public Store() {
		listOfServices = new ArrayList<>();
		
		int limitIdImages = PropertiesController.getPropertieInt("qtImages");
		idImage = ((new Random()).nextInt(limitIdImages))+1;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setId(String id) {
		if (id != null)
			this.id = Integer.parseInt(id.trim());
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public float getCoordX() {
		return coordX;
	}
	public void setCoordX(float coordX) {
		this.coordX = coordX;
	}
	public float getCoordY() {
		return coordY;
	}
	public void setCoordY(float coordY) {
		this.coordY = coordY;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getComplement() {
		return complement;
	}
	public void setComplement(String complement) {
		this.complement = complement;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getDistanceFromDestiny() {
		return distanceFromDestiny;
	}
	public void setDistanceFromDestiny(double distX, double distY) {
		this.distanceFromDestiny = CalcUtils.distance(distX, this.coordX, distY, this.coordY);
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public void setCategory(int id, String categoryName) {
		this.category = new Category(id, categoryName);
	}

	public void setCategory(int id, String categoryName, int imageId) {
		this.category = new Category(id, categoryName);
		this.category.setImageId(imageId);
	}
	public Category getCategory() {
		return category;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	//Obtem o JSON dos serviços cadastrados
	public String getJSONServices() {
		String strToReturn = "";
		if (!listOfServices.isEmpty()) {
			StringBuilder jsonService = new StringBuilder("{");
			for (int index = 0; index < listOfServices.size(); index++) {
				jsonService.append(listOfServices.get(index));
				if (index < listOfServices.size()-1) {
					jsonService.append(",");
				}
			}
		}
		return strToReturn;
	}
	public JSONObject toJSONObject() {
		
		JSONObject json = new JSONObject();
		json.put("id", this.id);
		json.put("title", this.title);
		json.put("cnpj", this.cnpj);
		json.put("introduction", this.introduction);
		json.put("description", this.description);
		json.put("email", this.email);
		json.put("tel", this.telefone);
		json.put("score", CalcUtils.roundNumber(this.score,2));
		json.put("countRatings", this.countAvaliable);
		json.put("valid", this.valid);
		json.put("priceLvl", this.priceLvl);
		json.put("createDate", this.createDate.toString());
		json.put("imageId", this.idImage);
		
		JSONObject adress = new JSONObject();
		adress.put("adress", this.adress);
		adress.put("complement", this.complement);
		adress.put("cep", this.cep);
		
		json.put("adress", adress);
		
		JSONObject coordinates = new JSONObject();
		coordinates.put("x", this.getCoordX());
		coordinates.put("y", this.getCoordY());

		json.put("coordinates", coordinates);
		json.put("distance", distanceFromDestiny==-1?null:CalcUtils.metersToKm(distanceFromDestiny));
		
		JSONArray servicesJson = new JSONArray();
		if (listOfServices != null) {
			for (Service service : listOfServices) {
				servicesJson.add(service.toJSONObject());
			}
		}
		json.put("services", servicesJson);
		json.put("category", category.toJSONObject());
		
		return json;
	}

	public ArrayList<Service> getListOfServicesToAdd() {
		return listOfServicesToAdd;
	}

	public void setListOfServicesToAdd(ArrayList<Service> listOfServicesToAdd) {
		this.listOfServicesToAdd = listOfServicesToAdd;
	}
	
	public ArrayList<Service> getListOfServices() {
		return listOfServices;
	}

	public void setListOfServices(ArrayList<Service> listOfServices) {
		this.listOfServices = listOfServices;
	}

	public int getPriceLvl() {
		return priceLvl;
	}

	public void setPriceLvl(int priceLvl) {
		this.priceLvl = priceLvl;
	}

	public int getCountAvaliable() {
		return countAvaliable;
	}

	public void setCountAvaliable(int countAvaliable) {
		this.countAvaliable = countAvaliable;
	}

	public int getIdImage() {
		return idImage;
	}

	public void setIdImage(int idImage) {
		this.idImage = idImage;
	}
	
}
