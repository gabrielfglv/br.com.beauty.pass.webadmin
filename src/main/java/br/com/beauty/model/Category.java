package br.com.beauty.model;

import org.json.simple.JSONObject;

public class Category {
	
	private int id
			,imageId = -1;
	private String name;
	
	public Category(int id) {
		this.id = id;
	}
	
	public Category(String id) {
		this.id = Integer.parseInt(id);
	}
	
	public Category(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public JSONObject toJSONObject() {
		
		JSONObject jsonToReturn = new JSONObject();

		jsonToReturn.put("id", id);
		jsonToReturn.put("title", name);
		jsonToReturn.put("imageId", imageId);
		
		return jsonToReturn;
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
}
