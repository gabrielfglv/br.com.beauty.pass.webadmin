package br.com.beauty.model;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import commons.CalcUtils;
import commons.StringUtils;

public class Service {

	private String title;
	private int id,
				position,
				vacanciesByTime,
				idStore;
	private String description;
	private double value;
	private boolean valid,
					hightlight,
					multiOption;
	private Category segment;
	private Category typeService;
	private ArrayList<ServiceOption> serviceOption;
	private Time timeStartService,
				timeEndService,
				duration;
	private JSONArray daysOfWeek;
	private Date createDate;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getId() {
		return id;
	}
	public void setId(String id) {
		this.id = Integer.parseInt(id);
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	public boolean hasDayOfWeek(int day) {
		return daysOfWeek.toString().contains(Integer.toString(day));
	}
	
	@Override
	public String toString() {
		return id+":{name:\""+title+"\", description:\""+description+"\",value:"+value+
				", valid:"+valid+"}";
	}
	public JSONObject toJSONObject() {
		JSONObject jsonToReturn = new JSONObject();
		jsonToReturn.put("id", id);
		jsonToReturn.put("title",title);
		jsonToReturn.put("description",description);
		jsonToReturn.put("position",position);
		jsonToReturn.put("valid",valid);
		jsonToReturn.put("multiOptions",multiOption);
		jsonToReturn.put("durationTime",CalcUtils.timeToMiliSeconds(duration));
		jsonToReturn.put("highlight",hightlight);
		jsonToReturn.put("type",typeService==null?null:typeService.toJSONObject());
		jsonToReturn.put("segment",segment==null?null:segment.toJSONObject());
		
		JSONArray jsonArrayOptions = new JSONArray();
		if (serviceOption != null) {
			for (ServiceOption serviceOptionItem : serviceOption) {
				jsonArrayOptions.add(serviceOptionItem.getJSONObject());
			}
		}

		jsonToReturn.put("options",jsonArrayOptions);
		jsonToReturn.put("optionsSize", serviceOption==null?0:serviceOption.size());
		return jsonToReturn;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public boolean isHightlight() {
		return hightlight;
	}
	public void setHightlight(boolean hightlight) {
		this.hightlight = hightlight;
	}
	public boolean isMultiOption() {
		return multiOption;
	}
	public void setMultiOption(boolean multiOption) {
		this.multiOption = multiOption;
	}
	public Category getTypeService() {
		return typeService;
	}
	public void setTypeService(int typeService) {
		this.typeService = new Category(typeService);
	}
	public void setTypeService(Category typeService) {
		this.typeService = typeService;
	}
	public ArrayList<ServiceOption> getServiceOption() {
		return serviceOption;
	}
	public void setServiceOption(ArrayList<ServiceOption> serviceOption) {
		this.serviceOption = serviceOption;
	}
	public Category getSegment() {
		return segment;
	}
	public void setSegment(Category segment) {
		this.segment = segment;
	}
	public void setSegment(int segmentId) {
		this.segment = new Category(segmentId);
	}
	public int getVacanciesByTime() {
		return vacanciesByTime;
	}
	public void setVacanciesByTime(int vacanciesByTime) {
		this.vacanciesByTime = vacanciesByTime;
	}
	public JSONArray getDaysOfWeek() {
		return daysOfWeek;
	}
	public ArrayList<Integer> getArrDaysOfWeesk() {
		ArrayList<Integer> arrWeek = new ArrayList<Integer>();
		for(int index = 0; index < daysOfWeek.size(); index++)
			arrWeek.add(Integer.parseInt(daysOfWeek.get(index).toString()));
		return arrWeek;
	}
	public void setDaysOfWeek(JSONArray daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}
	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = StringUtils.toJSONArr(daysOfWeek);
	}
	public Time getTimeStartService() {
		return timeStartService;
	}
	public String getTimeStartServiceStr() {
		return StringUtils.getTimeFormated(timeStartService);
	}
	public void setTimeStartService(Time timeStartService) {
		this.timeStartService = timeStartService;
	}
	public Time getTimeEndService() {
		return timeEndService;
	}
	public String getTimeEndServiceStr() {
		return StringUtils.getTimeFormated(timeEndService);
	}
	public void setTimeEndService(Time timeEndService) {
		this.timeEndService = timeEndService;
	}
	public Time getDuration() {
		return duration;
	}
	public String getDurationStr() {
		return StringUtils.getTimeFormated(duration);
	}
	public void setDuration(Time duration) {
		this.duration = duration;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getIdStore() {
		return idStore;
	}
	public void setIdStore(int idStore) {
		this.idStore = idStore;
	}
}
