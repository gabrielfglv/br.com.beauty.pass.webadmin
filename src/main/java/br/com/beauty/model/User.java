package br.com.beauty.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.json.simple.JSONObject;

/**
 * 
 * Classe responsavel por conter os atributos do Objeto Cliente
 * 
 */
@XmlRootElement
public final class User {

	private Integer id;
	private String nome;
	private String email;
	private String senha;
	private String cpf;
	private String endereco;
	private String cep;
	private Integer tipo;
	private boolean valido;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public boolean isValido() {
		return valido;
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public String getEmail() {
		return email;
	}

	public String getCep() {
		return cep;
	}

	public JSONObject toJSONObject() {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("id", id);
		jsonObj.put("name", nome);
		return jsonObj;
	}
	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nome=" + nome + ", cpf=" + cpf + ", endereco=" + endereco + "]";
	}
	
}
