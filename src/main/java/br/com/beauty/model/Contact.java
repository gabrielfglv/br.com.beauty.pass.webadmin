package br.com.beauty.model;

import java.sql.Date;

public class Contact {

	private int id,
				idServiceExecuted;
	private String message;
	private User user;
	private Date dateSend;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdServiceExecuted() {
		return idServiceExecuted;
	}
	public void setIdServiceExecuted(int idServiceExecuted) {
		this.idServiceExecuted = idServiceExecuted;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getDateSend() {
		return dateSend;
	}
	public void setDateSend(Date dateSend) {
		this.dateSend = dateSend;
	}
	


}
