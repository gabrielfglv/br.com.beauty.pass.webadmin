package br.com.beauty.model;

import java.sql.Timestamp;
import java.util.ArrayList;

import commons.StringUtils;


public class TimeService {
	
	private Timestamp timeDay;
	private ArrayList<Timestamp> listOfTimes;
	
	public Timestamp getTime() {
		return timeDay;
	}
	public void setTime(Timestamp time) {
		this.timeDay = time;
	}
	public ArrayList<Timestamp> getListOfTimes() {
		return listOfTimes;
	}
	public void setListOfTimes(ArrayList<Timestamp> listOfTimes) {
		this.listOfTimes = listOfTimes;
	}
	public String getDateString() {
		return StringUtils.getDateFormated(timeDay);
	}
	public String getDayOfWeek() {
		return StringUtils.getDayOfWeek(timeDay.getDay());
	}
	public String getCompleteDate() {
		return StringUtils.getCompleteDate(timeDay);
	}

}
