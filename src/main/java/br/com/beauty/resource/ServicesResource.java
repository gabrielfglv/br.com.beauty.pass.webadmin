package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import br.com.beauty.manager.ServiceController;
import br.com.beauty.manager.StoreController;
import br.com.beauty.model.Category;
import br.com.beauty.model.Service;
import br.com.beauty.model.Store;
import commons.StoreUtils;

/**
 * Manipula dados sobre os serviços de uma loja
 * @author aloiola
 *
 */
public class ServicesResource extends HttpServlet {

	/**
	 * Edita/Deleta serviços de uma loja
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        //Status
        String status = "Erro inesperado";
        //boolean errorAdd = true, added = false;
        boolean errorEdit = true, edited = false;
        boolean errorDelete = true, deleted = false;;

        String idStr = request.getParameter("id");
        //String servicesToAdd = request.getParameter("servicesToAdd");
        String servicesToEdit = request.getParameter("servicesToEdit");
        String servicesToDelete = request.getParameter("servicesToDelete");
        ServiceController controller = ServiceController.getInstance();
        
    	if (idStr != null) {
	        int id = Integer.parseInt(idStr);
    		
    		/*if (servicesToAdd != null && !servicesToAdd.replace("[]", "").isEmpty()) {
	        	ArrayList<Service> listOfServicesToAdd = StoreUtils.getServicesByJSONAdd(servicesToAdd);
	        	errorAdd = controller.addService(listOfServicesToAdd, id);
	        	added = true;
	        }*/
    		
    		if (servicesToEdit != null && !servicesToEdit.replace("{}", "").isEmpty()) {
    			ArrayList<Service> listOfServicesToEdit = StoreUtils.getServicesByJSONEdit(servicesToEdit);
    			int rowsModify = controller.editService(listOfServicesToEdit, id);
    			if (rowsModify == -1) {
    				errorEdit = true;
    			}else if (rowsModify > 0) {
    				edited = true;
    				errorEdit = false;
    			}
    		}
    		
    		if (servicesToDelete != null && !servicesToDelete.replace("[]", "").isEmpty()) {
    			ArrayList<String> listOfServiceToDelete = StoreUtils.getIdsByJSONDelete(servicesToDelete);
    			errorDelete =  controller.removeServices(listOfServiceToDelete, id);
    			deleted = true;
    		}
    	}
        
        
        try (PrintWriter writer = response.getWriter()) {

            writer.println("<!DOCTYPE html><html>");
            writer.println("<head>");
            writer.println("<meta charset=\"UTF-8\" />");
            writer.println("<title>Beauty Pass</title>");
            writer.println("</head>");
            writer.println("<body>");
            writer.println("<a href=\"/services/servicos.jsp?storeId="+idStr+"\">voltar</a>");
            writer.println("<h1>Edição de serviços</h1>");
            if (!deleted && !edited) {
                writer.println("<p>Não houve alterações</p>");
            }else {
            	/*if (added) {
		            if (errorAdd) {
		            	writer.println("<p>Erro ao adicionar serviços</p>");
		           	}else {
		           		writer.println("<p>Serviços adicionados com sucesso!</p>");
		           	}	
		        }*/
            	if (edited) {
	            	if (errorEdit) {
	            		writer.println("<p>Erro ao editar os serviços</p>"); 
					}else {
	            		writer.println("<p>Serviços editados com sucesso!</p>");
	            	}
	            }	
          		if (deleted) {
	            	if (errorDelete) {
	            		writer.println("<p>Erro ao deletar os serviços</p>");
	            	}else {
	            		writer.println("<p>Serviços deletados com sucesso!</p>");
	            	}
            	}
            }
            
            writer.println("</body>");
            writer.println("</html>");
        }
        
    }
	
	
	/**
	 * Obtem os dados dos serviços de uma loja
	 */
	@Override
	 public void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws IOException {

		response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
	
        boolean sucess = false;
	    JSONObject jsonResponse = new JSONObject();
        String idStr = request.getHeader("idStore");
        if (idStr != null) {
        	
        	int id = -1;
        	ServiceController controller = ServiceController.getInstance();
        	
        	try {
        		id = Integer.parseInt(idStr);
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        	
        	if (id != -1) {
        		
        		jsonResponse.put("idStore", id);
        		ArrayList<Service> listOfServices = controller.getServicesAndOptions(id);
        		if (listOfServices != null) {
	        		JSONArray jsonArray = new JSONArray();
	        		for (Service service : listOfServices) {
	        			jsonArray.add(service.toJSONObject());
	        		}
	        		jsonResponse.put("services", jsonArray);
	        		jsonResponse.put("sizeServices", listOfServices.size());
	        		sucess = true;
        		}
        		
        	}
        }
        
        jsonResponse.put("sucess", sucess);
		
        PrintWriter out = response.getWriter();
	    response.setContentType("application/json;charset=iso-8859-1");
	    response.setCharacterEncoding("iso-8859-1");
	    out.print(jsonResponse.toJSONString());
	    out.flush();
		
	}
	
}
