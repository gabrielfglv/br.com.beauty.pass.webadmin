package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import br.com.beauty.manager.CalendarController;
import br.com.beauty.manager.StoreController;

/**
 * Atualiza o status de um serviço (Confirmado/Negado)
 * @author aloiola
 *
 */
public class StatusService extends HttpServlet {
	   
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        response.setContentType("text/html");
	        response.setCharacterEncoding("UTF-8");
		
	        boolean sucess = false;
	        String idStr = request.getParameter("id");
	        String statusStr = request.getParameter("status");
	        if (idStr != null) {
	        	try {
	        		int id = Integer.parseInt(idStr);
	        		int status = Integer.parseInt(statusStr);
	    			
	        		CalendarController controller = CalendarController.getInstance();
	    			sucess = controller.updateServiceStatus(id, status);
	    			
	        	}catch(Exception e) {
	        		e.printStackTrace();
	        	}
	        }
	        
	        PrintWriter out = response.getWriter();
		    response.setContentType("application/json;charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    JSONObject jsonResponse = new JSONObject();
		    jsonResponse.put("sucesso", sucess);
		    out.print(jsonResponse.toJSONString());
		    out.flush();
		}

}
