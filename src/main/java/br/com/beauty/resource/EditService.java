package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.beauty.manager.ServiceController;
import br.com.beauty.manager.StoreController;
import br.com.beauty.model.Service;
import br.com.beauty.model.ServiceOption;
import br.com.beauty.model.Store;
import commons.ServiceUtils;
import commons.StoreUtils;

/**
 * Edita um serviço
 * @author aloiola
 *
 */
public class EditService  extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        
        Service serviceToEdit = ServiceUtils.getServiceByRequest(request);
        ServiceController controller = ServiceController.getInstance();
        
        boolean sucessAdd = false, added = false;
        boolean sucessDelete = false, deleted = false;
        boolean sucessEdit = false, edited = false;
        boolean genericError = false;
        
        String status = "Erro inesperado";
        if (serviceToEdit != null){
	       
        	//Edita o serviço
        	int qtResults = controller.editService(serviceToEdit);
        	if (qtResults <= 0) {
        		status = "Erro ao editar os dados";
        		genericError = true;
        	}else {
        	
	        	//Options to add
	        	String arrOptionsAdd = request.getParameter("optionsToAdd");
	        	if (arrOptionsAdd != null && !arrOptionsAdd.replace("[]", "").isEmpty()) {
	        		ArrayList<ServiceOption> listOfServiceOptions = ServiceUtils.getServiceOptions(arrOptionsAdd);
	        		if (listOfServiceOptions != null && !listOfServiceOptions.isEmpty()) {
	        			sucessAdd = controller.addServiceOptions(listOfServiceOptions, serviceToEdit.getId());
	        			added = true;
	        		}
	        	}
	        	
	        	//Options to Edit
	        	String arrOptionsEdit = request.getParameter("optionsToEdit"); 
	        	if (arrOptionsEdit != null && !arrOptionsEdit.replace("[]", "").isEmpty()) {
	        		ArrayList<ServiceOption> listOfServiceOptionsEdit = ServiceUtils.getServiceOptions(arrOptionsEdit);
	        		if (listOfServiceOptionsEdit != null && !listOfServiceOptionsEdit.isEmpty()) {
	        			sucessEdit = controller.updateServiceOptions(listOfServiceOptionsEdit);
	        			edited = true;
	        		}
	        	}
	        	
	        	//Options to delete
	        	String arrOptionsDelete = request.getParameter("optionsToDelete"); 
	        	if (arrOptionsDelete != null && !arrOptionsDelete.isEmpty()) {
	        		List<String> idsToDelete = Arrays.asList(arrOptionsDelete.split(","));
	        		if (idsToDelete != null && !idsToDelete.isEmpty()) {
	        			sucessDelete = controller.deleteServiceOptions(idsToDelete);
	        			deleted = true;
	        		}
	        	}
	        	
        	}
        }else {
        	status = "Erro ao obter os dados do serviço";
        }

        try (PrintWriter writer = response.getWriter()) {

            writer.println("<!DOCTYPE html><html>");
            writer.println("<head>");
            writer.println("<meta charset=\"UTF-8\" />");
            writer.println("<title>Loja editada</title>");
            writer.println("</head>");
            writer.println("<body>");
            if (serviceToEdit != null) {
            	writer.println("<a href=\"/services/servicos.jsp?storeId="+serviceToEdit.getIdStore()+"\">voltar</a>");
            }else {
            	writer.println("<a href=\"/\">voltar</a>");
            }
            
            if (genericError) {
            	writer.println("<p>Houve um erro: "+status+"</p>");	
            }else {
            	writer.println("<p>Sucesso ao editar o serviço</p>");
            }
            
            if (added) {
            	if (sucessAdd) {
            		writer.println("<p>Novas opções adicionadas com sucesso</p>");
            	}else {
            		writer.println("<p>Houve um erro ao adicionar as novas opções</p>");
            	}
            }
            

            if (edited) {
            	if (sucessEdit) {
            		writer.println("<p>Opções editadas com sucesso</p>");
            	}else {
            		writer.println("<p>Houve um erro ao editar as opções</p>");
            	}
            }
            
            if (deleted) {
            	if (sucessDelete) {
            		writer.println("<p>Opções deletadas com sucesso</p>");
            	}else {
            		writer.println("<p>Houve um erro ao deletar as opções</p>");
            	}
            }
            
            
            writer.println("</body>");
            writer.println("</html>");
        }
        
        
    }


}
