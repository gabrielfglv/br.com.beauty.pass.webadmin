package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.beauty.manager.ServiceController;
import br.com.beauty.model.Service;
import br.com.beauty.model.ServiceOption;
import commons.ServiceUtils;

/*
 * Adiciona um novo serviço
 */
public class AddService extends HttpServlet {
	   
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        response.setContentType("text/html");
	        response.setCharacterEncoding("UTF-8");

	        //Status
	        String status = "Erro inesperado";
	        boolean error = true;
	        
	        Service serviceToInsert = ServiceUtils.getServiceByRequest(request);
	        ServiceController controller = ServiceController.getInstance();
	        
	        String idStr = request.getParameter("storeId");
	        
	        if (idStr != null) {
		        int id = Integer.parseInt(idStr);
		        if (serviceToInsert != null) {
		        	
		        	String arrOptions = request.getParameter("optionsToAdd");
		        	if (arrOptions != null && !arrOptions.replace("[]", "").isEmpty()) {
		        		ArrayList<ServiceOption> listOfServiceOptions = ServiceUtils.getServiceOptions(arrOptions);
		        		if (listOfServiceOptions != null && !listOfServiceOptions.isEmpty()) {
		        			serviceToInsert.setServiceOption(listOfServiceOptions);
		        		}
		        	}
		        	
		        	error = !controller.addService(serviceToInsert, id);
		        }
	        }
		    
	        try (PrintWriter writer = response.getWriter()) {

	            writer.println("<!DOCTYPE html><html>");
	            writer.println("<head>");
	            writer.println("<meta charset=\"UTF-8\" />");
	            writer.println("<title>Beauty Pass</title>");
	            writer.println("</head>");
	            writer.println("<body>");
	            writer.println("<a href=\"/services/servicos.jsp?storeId="+idStr+"\">voltar</a>");
	            
	            if (error) {
	                writer.println("<h1>Houve um erro</h1>");
	                writer.println("<p>Houve um erro ao processar o cadastro do serviço: "+status+"</p>");
	            }else {
	            	writer.println("<h1>Serviço inserido com sucesso!</h1>");
	            }
	            
	            writer.println("</body>");
	            writer.println("</html>");
	        }
	        
	        
	    }

	}