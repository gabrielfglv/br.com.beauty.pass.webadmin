package br.com.beauty.resource;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import br.com.beauty.manager.PropertiesController;
import br.com.beauty.manager.StoreController;
import br.com.beauty.model.Store;

/**
 * Obtem uma lista de lojas de acordo com os parametros
 * @author aloiola
 *
 */
public class GetStores extends HttpServlet {
	  
	@Override
	 public void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws IOException {

			JSONObject jsonResponse = new JSONObject();
			StoreController controller = StoreController.getInstance();
			
			boolean sucess = true,storesSearchTxt = false;
			String statusMessage = "sucesso";

			//Coordenadas opcionais
			Object coordXObj = request.getHeader("coordenada-x");
			Object coordYObj = request.getHeader("coordenada-y");

			//Busca por texto
			Object searchTextObj = request.getHeader("search");

			ArrayList<Store> listOfStores = null;

			//Valida se há busca filtrada
			if (coordXObj != null && coordYObj != null) {
				
				try {
					
					float coordX = Float.parseFloat(coordXObj.toString());
					float coordY = Float.parseFloat(coordYObj.toString());
					
					//Valida se é necessário realizar uma busca pela string recebida
					if (searchTextObj == null) {
						listOfStores = controller.getListOfStores(coordX, coordY);
					}else {
						listOfStores = controller.getListOfStoresByTitle(coordX, coordY, searchTextObj.toString());
					}
					
				}catch(NumberFormatException e) {
					sucess = false;
					statusMessage = "Erro ao obter os dados das coordenadas no servidor";
				}
				
			}else{
				listOfStores = controller.getListOfStores();
			}
			
			
			//Valida se há retorno
			if (listOfStores != null) {
				
					JSONArray jsonArray = new JSONArray();
					
					//Forma o JSON de retorno
					for (Store store : listOfStores) {
						jsonArray.add(store.toJSONObject());
					}
					
					JSONObject properties = new JSONObject();
					
					//Propriedades
					float range = PropertiesController.getPropertieFloat("raioDasLojas");
					
					properties.put("rangeStores", range);
					
					
					if (searchTextObj == null) {
						int limitHighlightHome = PropertiesController.getPropertieInt("limiteDestaque");
						int limitTypesServiceHome = PropertiesController.getPropertieInt("limiteTiposLojaDestaque");
						
						properties.put("limitHighlightHome", limitHighlightHome);
						properties.put("limitTypesServiceHome", limitTypesServiceHome);
					}else if (storesSearchTxt) {
						int limitServicesByStore = PropertiesController.getPropertieInt("limiteServiçosBusca");
						properties.put("limitSearchServices", limitServicesByStore);
					}
					
					jsonResponse.put("properties", properties);
					jsonResponse.put("stores", jsonArray);
					jsonResponse.put("size", listOfStores.size());
				
			}else {
				sucess = false;
				statusMessage = "Erro ao processar os dados das lojas no servidor";
			}

			jsonResponse.put("sucess", sucess);
			jsonResponse.put("responseTxt", statusMessage);
			
		    
			PrintWriter out = response.getWriter();
		    response.setContentType("application/json;charset=UTF-8");
		    response.setCharacterEncoding("UTF-8");
		    out.print(jsonResponse.toJSONString());
		    out.flush();
	  }
	
}
