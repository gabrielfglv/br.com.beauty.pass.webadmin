package br.com.beauty.manager;

import java.util.ArrayList;

import br.com.beauty.dao.CalendarDAO;
import br.com.beauty.dao.ContactDAO;
import br.com.beauty.model.Contact;

public class ContactController {

	private static ContactController instance;
	private static ContactDAO contactDao;
	
	public static ContactController getInstance() {
		if (instance == null)
			instance = new ContactController();
		return instance;
	}
	
	public ContactController() {
		contactDao = new ContactDAO();
	}
	
	public boolean insertContact(Contact contact) {
		LoggerController.getInstance().insertLog("Foi recebido um novo contato do usuario que marcou o serviço do id "+contact.getIdServiceExecuted());
		return contactDao.insertContact(contact);
	}
	
	public ArrayList<Contact> listOfContacts(){
		return contactDao.getListOfContact();
	}
	
}
