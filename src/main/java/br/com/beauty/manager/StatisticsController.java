package br.com.beauty.manager;

import br.com.beauty.dao.ServiceDAO;
import br.com.beauty.dao.StatisticsDAO;
import br.com.beauty.model.Statistic;

public class StatisticsController {
	
	private static StatisticsController instance;
	private static StatisticsDAO statisticsdao;
	
	public static StatisticsController getInstance() {
		if (instance == null)
			instance = new StatisticsController();
		return instance;
	}
	
	public StatisticsController() {
		statisticsdao = new StatisticsDAO();
	}
	
	public Statistic getStatitics(int idStore) {
		return statisticsdao.getStatisticStore(idStore);
	}
	
}
