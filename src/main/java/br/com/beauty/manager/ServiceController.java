package br.com.beauty.manager;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import br.com.beauty.dao.ServiceDAO;
import br.com.beauty.dao.StoreDAO;
import br.com.beauty.model.Category;
import br.com.beauty.model.Service;
import br.com.beauty.model.ServiceOption;
import br.com.beauty.model.TimeService;
import commons.CalcUtils;
import commons.StringUtils;

public class ServiceController {

	private static ServiceController instance;
	private static ServiceDAO serviceDao;
	
	public static ServiceController getInstance() {
		if (instance == null)
			instance = new ServiceController();
		return instance;
	}
	
	public ServiceController() {
		serviceDao = new ServiceDAO();
	}
	
	public boolean addService(Service service, int id) {
		boolean sucess = false;
		if (service != null)
			sucess = serviceDao.insertServices(service, id);
		LoggerController.getInstance().insertLog("Foi acrecentado um novo serviço na loja do id "+service.getIdStore()+", sucesso: "+sucess);
		return sucess;
	}
	
	public int editService(ArrayList<Service> services, int id) {
		int rowsModify = -1;
		if (services != null && !services.isEmpty())
			rowsModify = serviceDao.updateServicesShort(services, id);
		LoggerController.getInstance().insertLog("A loja do id: "+id+" teve seus serviços alterados, quantidade: "+rowsModify);
		return rowsModify;
	}
	
	public int editService(Service service) {
		int rowsModify = serviceDao.updateService(service);
		LoggerController.getInstance().insertLog("A loja do id: "+service.getIdStore()+" teve seu serviço do id "+service.getId()+" alterado, sucesso:"+(rowsModify>0));
		return rowsModify;
	}
	
	public boolean addServiceOptions(ArrayList<ServiceOption> listOfOptions, int idService) {
		boolean sucess = false;
		if (listOfOptions != null)
			sucess = serviceDao.insertOptionsService(listOfOptions, idService);
		return sucess;
	}
	
	public boolean updateServiceOptions(ArrayList<ServiceOption> listOfOptions) {
		int rowsUpdated = 0;
		if (listOfOptions != null)
			rowsUpdated = serviceDao.updateServicesOptions(listOfOptions);
		return rowsUpdated > 0;
	}
	
	public boolean deleteServiceOptions(List<String> listOfIds) {
		return serviceDao.removeServiceOptions(listOfIds);
	}
	
	public ArrayList<Service> getServices(int idStore){
		return serviceDao.getServices(idStore, false); 
	}
	
	public Service getService(int id){
		return serviceDao.getService(id); 
	}
	
	public ArrayList<Service> getServicesAndOptions(int idStore){
		return serviceDao.getServices(idStore,true); 
	}
	
	public boolean removeServices(ArrayList<String> listOfIdServices, int storeId) {
		return serviceDao.removeServices(listOfIdServices, storeId);
	}
	
	public HashMap<Integer, Category> getHashByListCategory(ArrayList<Category> listOfCategorys){
		HashMap<Integer, Category> hashTypes = new HashMap<>();
		
		for (Category category : listOfCategorys) {
			hashTypes.put(category.getId(), category);
		}
		
		return hashTypes;
	}
	
	public ArrayList<Category> getTypeOfServices(){
		return serviceDao.getListOfCategorys();
	}
	
	public boolean insertSegments(ArrayList<String> listOfSegments, int idStore) {
		return serviceDao.insertSegmets(listOfSegments, idStore);
	}
	
	public ArrayList<Category> getSegmentsStore(int idLoja){
		return serviceDao.getListOfSegments(idLoja);
	}
	
	public boolean removeSegments(ArrayList<String> idsToRemove) {
		return serviceDao.removeSegments(idsToRemove);
	}
	
	public boolean editSegments(ArrayList<Category> listOfSegments, int idStore) {
		return serviceDao.updateSegments(listOfSegments, idStore);
	}
	
	public HashMap<Long, TimeService> getCalendarServiceDisp(int serviceId) throws Exception{
		
		Service service = serviceDao.getService(serviceId);
		
		HashMap<Long, TimeService> list = null;
		
		if (service != null) {
		
			CalendarController controllerCalendar = CalendarController.getInstance();
			
			HashMap<Long, Integer> listOfUsedDate = controllerCalendar.getUsedDates(serviceId);
			
			if (listOfUsedDate != null) {
				
				int limitDate = PropertiesController.getPropertieInt("limitDiasCadatro");
				list = new HashMap<>();

				Calendar calendar = controllerCalendar.getToday();
				
				ArrayList<Integer> daysOfWeek = service.getArrDaysOfWeesk();
	
				Timestamp current = new Timestamp(calendar.getTime().getTime());
				Timestamp currentDate = new Timestamp(calendar.getTime().getTime());
				
				for (int dayIndex = 0; dayIndex < limitDate; dayIndex++) {
					
					TimeService newTimeService = new TimeService();
					
					current.setHours(service.getTimeStartService().getHours());
					current.setMinutes(service.getTimeStartService().getMinutes());
					current.setSeconds(0);
					current.setNanos(0);
					
					ArrayList<Timestamp> listOfTimes = new ArrayList<>();
					
					//Valida os dias das semanas
					if (daysOfWeek.contains(current.getDay())) {
					
						while (CalcUtils.isAfterTime(service.getTimeEndService(), current)){
							//Valida se está depois da data atual e se não há 
							//um serviço marcado nessa data
							
							if ((current.after(currentDate)
									|| CalcUtils.isAfterTime(current, currentDate))
									&& (!listOfUsedDate.containsKey(current.getTime())
										|| listOfUsedDate.get(current.getTime()) < service.getVacanciesByTime())){
								
								listOfTimes.add((Timestamp) current.clone());
								
							}
							current = CalcUtils.sumTime(current, service.getDuration());
						}
						
						newTimeService.setTime((Timestamp) current.clone());
						newTimeService.setListOfTimes(listOfTimes);
						list.put(current.getTime(), newTimeService);
					}
					
					//Acrescenta um dia
					current.setDate(current.getDate()+1);
				}
			}else {
				throw new Exception("Erro ao obter os dados das datas marcadas do serviço");	
			}
		}else {
			throw new Exception("Erro ao obter os dados do serviço");
		}
		return list;
	}
	
}
