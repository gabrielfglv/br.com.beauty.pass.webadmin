package br.com.beauty.manager;

import java.util.ArrayList;

import br.com.beauty.dao.UserDAO;
import br.com.beauty.dao.ServiceDAO;
import br.com.beauty.model.User;

public class UserController {

	private static UserController instance;
	private static UserDAO clientDAO;
	
	public static UserController getInstance() {
		if (instance == null)
			instance = new UserController();
		return instance;
	}
	
	public UserController() {
		clientDAO = new UserDAO();
	}
	
	public User insertUser(String ip, String localeUser) {
		LoggerController.getInstance().insertLog("Foi inserido um novo usuario", ip);
		return new UserDAO().insert(ip, localeUser);
	}

}
