<!doctype html>
<%@page import="br.com.beauty.model.Log,
		br.com.beauty.manager.LoggerController,
		java.util.ArrayList"%>
<html>
  <head>
  	<%@ include file="headImports.html"%>
    <title>Beauty Pass - Admin</title>
    <meta charset="utf-8"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  </head>
  <body>
  	<a href="/">voltar</a>
		<h1>LISTA DE LOGS</h1>
		<%
		ArrayList<Log> logList = LoggerController.getInstance().getLogs();

		 if (logList == null){
		%>	
			<h2>Erro ao consultar o banco de dados</h2>
			<h3>Contate o adminsitrador</h3>
		<%
		}else if (logList.size() == 0){
		%>
			<h2>Erro, n�o h� logs</h2>
		<%
		}else{
		%>
			<table style="width:100%" border="1">
			  <tr>
			  	<th>Mensagem</th>
			  	<th>Date</th>
			  </tr>
			  <%
			  
			  for (Log log: logList){
			  %>
			  <tr>
			   	<th><%=log.getMessage()%></th>
			   	<th><%=log.getDate()%></th>
			  </tr>
			  <%
			  }
			  %>
			</table>
		<%
		}%>
  </body>
</html>
