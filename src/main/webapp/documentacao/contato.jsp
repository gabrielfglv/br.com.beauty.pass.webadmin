<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<%@page import="br.com.beauty.manager.PropertiesController"%>
<html>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Beauty Pass - Admin</title>
    <meta charset="utf-8"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  </head>
  <body>
		<a href="/documentacao/documentacao.jsp">voltar</a>
		<h1>BEAUTY PASS - WEBSERVICE</h1>
		
		<br><h2>CHAMADAS REST</h2>
		
		<p>CONTATO</p>
		<p>Tipo: POST</p>
		<p>URL: <u>/contact</u></p>
		<p>DESCRIÇÃO: Insere uma nova mensagem</p>
		<p>Parâmetros:</p>
		<table style="width:100%" border="1">
			  <tr>
			    <th>Key</th>
			    <th>Tipo de dado</th>
			    <th>Obrigatorio</th>
			    <th style="width:40%">Descrição</th>
			  </tr>
			  <tr>
			    <th>id</th>
			    <th>Int</th>
			    <th>True</th>
			    <th>Id do serviço marcado</th>
			  </tr>
			  <tr>
			    <th>message</th>
			    <th>String</th>
			    <th>True</th>
			    <th>Mensagem à ser postada</th>
			  </tr>
		</table>
		<p>Exemplo de JSON para o POST: <a href="examples/postContact.json" target="_blank">postContact.json</a></p>
		<p>Retorno: <a href="examples/returnJsonContact.json" target="_blank">returnJsonContact.json</a></p>
  </body>
</html>
