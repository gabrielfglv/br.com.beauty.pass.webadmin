<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<%@page import="br.com.beauty.manager.PropertiesController"%>
<html>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Beauty Pass - Admin</title>
    <meta charset="utf-8"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  </head>
  <body>
		<a href="/documentacao/documentacao.jsp">voltar</a>
		<h1>BEAUTY PASS - WEBSERVICE</h1>
		
		<br><h2>CHAMADAS REST</h2>
		<br>
		<p>CONSULTA DE SERVIÇOS MARCADOS DE UM USER</p>
		<p>Tipo: GET</p>
		<p>URL: <u>/userCalendar</u></p>
		<p>DESCRIÇÃO: Obtem a agenda de um usuario</p>
		<p>Parâmetros:</p>
		<table style="width:100%" border="1">
			  <tr>
			    <th>Key</th>
			    <th>Tipo de dado</th>
			    <th>Obrigatorio</th>
			    <th style="width:40%">Descrição</th>
			  </tr>
			  <tr>
			    <th>userId</th>
			    <th>Int</th>
			    <th>True</th>
			    <th>Id do usuario</th>
			  </tr>
		</table>
		<p>Retorno: <a href="examples/calendarUser.json" target="_blank">calendarUser.json</a></p>
		<br>
		<p>CONSULTA DE HORARIOS DE UM SERVIÇO DE LOJA</p>
		<p>Tipo: GET</p>
		<p>URL: <u>/serviceCalendar</u></p>
		<p>DESCRIÇÃO: Obtem uma lista de horarios disponiveis de um serviço</p>
		<p>Parâmetros:</p>
		<table style="width:100%" border="1">
			  <tr>
			    <th>Key</th>
			    <th>Tipo de dado</th>
			    <th>Obrigatorio</th>
			    <th style="width:40%">Descrição</th>
			  </tr>
			  <tr>
			    <th>serviceId</th>
			    <th>Int</th>
			    <th>True</th>
			    <th>Id do serviço</th>
			  </tr>
		</table>
		<p>Retorno: <a href="examples/serviceDates.json" target="_blank">serviceDates.json</a></p>
		<br>
		
		<p>MARCA UM SERVIÇO</p>
		<p>Tipo: POST</p>
		<p>URL: <u>/serviceCalendar</u></p>
		<p>DESCRIÇÃO: Marca um serviço</p>
		<p>Parâmetros:</p>
		<table style="width:100%" border="1">
			  <tr>
			    <th>Key</th>
			    <th>Tipo de dado</th>
			    <th>Obrigatorio</th>
			    <th style="width:40%">Descrição</th>
			  </tr>
			  <tr>
			    <th>serviceId</th>
			    <th>Int</th>
			    <th>True</th>
			    <th>Id do serviço</th>
			  </tr>
			  <tr>
			    <th>dateTime</th>
			    <th>Long</th>
			    <th>True</th>
			    <th>Valor do dateTime em Long do horario</th>
			  </tr>
			  <tr>
			    <th>value</th>
			    <th>Double</th>
			    <th>True</th>
			    <th>Valor total do serviço</th>
			  </tr>
			  <tr>
			    <th>idUser</th>
			    <th>Int</th>
			    <th>True</th>
			    <th>Id do usuario</th>
			  </tr>
			  <tr>
			    <th>options</th>
			    <th>Array de String</th>
			    <th>True</th>
			    <th>JSON</th>
			  </tr>
		</table>
		<p>Exemplo de JSON para o POST: <a href="examples/postService.json" target="_blank">postService.json</a></p>
		<p>Retorno: <a href="examples/returnServidePosted.json" target="_blank">returnServidePosted.json</a></p>
  </body>
</html>
