<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<%@page import="br.com.beauty.manager.PropertiesController"%>
<html>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Beauty Pass - Admin</title>
    <meta charset="utf-8"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  </head>
  <body>
		<a href="/documentacao/documentacao.jsp">voltar</a>
		<h1>BEAUTY PASS - WEBSERVICE</h1>
		
		<br><h2>CHAMADAS REST</h2>
		<br>
		<p>CONSULTA DE SERVIÇOS DE LOJA</p>
		<p>Tipo: GET</p>
		<p>URL: <u>/serviceResource</u></p>
		<p>DESCRIÇÃO: Obtem a lista de serviços de uma loja</p>
		<p>Parâmetros:</p>
		<table style="width:100%" border="1">
			  <tr>
			    <th>Key</th>
			    <th>Tipo de dado</th>
			    <th>Obrigatorio</th>
			    <th style="width:40%">Descrição</th>
			  </tr>
			  <tr>
			    <th>idStore</th>
			    <th>Int</th>
			    <th>True</th>
			    <th>Id da loja à ser pesquisada</th>
			  </tr>
		</table>
		<p>Retorno: <a href="examples/services.json" target="_blank">services.json</a></p>
		<br>
		<p>AVALIA UM SERVIÇO</p>
		<p>Tipo: POST</p>
		<p>URL: <u>/ratingService</u></p>
		<p>DESCRIÇÃO: Avalia um serviço</p>
		<p>Parâmetros:</p>
		<table style="width:100%" border="1">
			  <tr>
			    <th>Key</th>
			    <th>Tipo de dado</th>
			    <th>Obrigatorio</th>
			    <th style="width:40%">Descrição</th>
			  </tr>
			  <tr>
			    <th>id</th>
			    <th>Int</th>
			    <th>True</th>
			    <th>Id do serviço</th>
			  </tr>
			  <tr>
			    <th>value</th>
			    <th>Int</th>
			    <th>True</th>
			    <th>Valor da avaliação</th>
			  </tr>
		</table>
		<p>Exemplo de JSON para o POST: <a href="examples/postJsonServiceRating.json" target="_blank">postJsonServiceRating.json</a></p>
		<p>Retorno: <a href="examples/returnJsonServiceRating.json" target="_blank">returnJsonServiceRating.json</a></p>
  </body>
</html>
