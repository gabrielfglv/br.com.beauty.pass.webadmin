<%@page import="commons.StringUtils"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.model.*,
		br.com.beauty.manager.*,
		br.com.commons.StringUtils,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Serviço agendado</title>
    <meta charset="utf-8"/>
  </head>
  <body>
  	<%
  		String idOnParameter = request.getParameter("id");
  		ScheduledService scheduledService = null;	
	  	
	  	if (idOnParameter != null){
	  		
	  		int id;
	  		try{
	  			id = Integer.parseInt(idOnParameter);
	  			scheduledService = CalendarController.getInstance().getSheduledServiceStore(id);
	  		}catch(NumberFormatException e){
	  			System.out.print("Erro ao obter os dados do id para editar a loja");
	  		}
	  	}
  		
  	%>
  <a href="#0" onclick="window.history.back();">voltar</a>
    <h1>Agenda de Serviços de loja</h1>
    <br/>
    <% 
    if (scheduledService != null){
    %>
	 ID: <input type="number" disabled value="<%=scheduledService.getId()%>">
	<br />
	Serviço: <input type="text" name="title" disabled value="<%=scheduledService.getNameService()%>">
	<br />
	Id do usuário: <input type="text" name="introduction" disabled value="<%=scheduledService.getClient().getId()%>"/>
	<br />
	Nome do usuário: <input type="text" name="description" disabled value="<%=scheduledService.getClient().getNome()%>"/>
	<br />
	Nome da loja: <input type="text" name="description" disabled value="<%=scheduledService.getStore().getTitle()%>"/>
	<br />
	Data: <input type="text" name="description" disabled value="<%= StringUtils.getDateFormated(scheduledService.getDate())%>"/>
	<br />
	Horario: <input type="text" name="introduction" disabled value="<%=StringUtils.getTimeFormated(scheduledService.getTime())%>"/>
	<br />
	Status: <input type="text" name="description" disabled value="<%=scheduledService.getConfirmedString()%>"/>
	<br />
	Avaliação: <input type="text" name="description" disabled value="<%=scheduledService.getScore()%>"/>
	<br />
	Total: <input type="text" name="introduction" disabled value="<%=scheduledService.getValue()%>"/>
	<br />
	Opções:
	 <table id="tableServices" style="width:100%" border="1">
		  <tr>
		  	<th>Id</th>
		  	<th>Opção</th>
		  	<th>Valor</th>
		  </tr>
		  <%
		  for (ServiceOption option : scheduledService.getListOfOptions()){
		  %>
		   <tr>
		   	   <th><%=option.getId()%></th>
		   	   <th><%=option.getTitle()%></th>
		   	   <th><%=option.getValor()%></th>
		 	</tr>
		  <%
		  }
		  %>
		</table>
	
    <%
    }else{ %>
    	<h2>Erro ao obter os dados</h2>
    <%} %>
   </body>
</html>