<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.model.*,
		br.com.beauty.manager.*,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Agenda</title>
    <meta charset="utf-8"/>
  </head>
  <body>
  	<%
  		String idOnParameter = request.getParameter("storeId");
	  	Store store = null;	
	  	
	  	if (idOnParameter != null){
	  		
	  		int id;
	  		try{
	  			id = Integer.parseInt(idOnParameter);
	  			store = StoreController.getInstance().getStore(id);	
	  		}catch(NumberFormatException e){
	  			System.out.print("Erro ao obter os dados do id para editar a loja");
	  		}
	  	}
  		
  	%>
  <a href="/listaDeLojas.jsp">voltar</a>
    <h1>Agenda de Serviços de loja</h1>
    <h2>Loja: <%=store.getTitle()%></h2>
    <br/>
    <% 
    if (store != null){
    	
    	ArrayList<ScheduledService> listOfServices = CalendarController.getInstance().getSheduledServicesStore(store.getId());	
    	
    	if (listOfServices != null && !listOfServices.isEmpty()){
    %></br>
      <table id="tableServices" style="width:100%" border="1">
		  <tr>
		  	<th >id</th>
		  	<th>Serviço</th>
		  	<th>Id user</th>
		  	<th>Nome user</th>
		  	<th>Valor</th>
		  	<th>Data</th>
		  	<th>Hora</th>
		  	<th>Status</th>
		  	<th>Ação</th>
		  	<th>Detalhes</th>
		  </tr>
		  <%
		  for (ScheduledService service : listOfServices){
		  %>
		   <tr>
		   	   <th><%=service.getId()%></th>
		   	   <th><%=service.getNameService()%></th>
		   	   <th><%=service.getClient().getId()%></th>
		   	   <th><%=service.getClient().getNome()%></th>
		   	   <th><%=service.getValue()%></th>
		   	   <th><%=service.getFormatedDate()%></th>
		   	   <th><%=service.getFormatedTime()%></th>
		   	   <th class="status"><%=service.getConfirmedString()%></th>
		   	   <%if (service.getConfirmed()==-1 && !service.isExecuted()){ %>
		   	   <th><button class="confirm">Confirmar</button><button class="deny">Negar</button></th>
		   	   <%}else{ %>
		   	   <th></th>
		   	   <%} %>
		   	   <th><a href="/servicoAgendado.jsp?id=<%=service.getId()%>">Detalhes</a></th>
		 	</tr>
		  <%
		  }
		  %>
		</table>
		<script>
			$("button").click(function() {
	    		var id = $(this).parent().parent().find("th")[0].innerText;
	    		var value = $(this).hasClass("confirm")?0:1;
	    		
	    		var confirmResponse = confirm("Você realmente deseja alterar o agendamento do id "+id+
	    				" para "+(value==0?"confirmado":"rejeitado")+"?");
	    		if (confirmResponse == true) {
	    			changeStatus(id, value, this);
	    		} 
	    		
	    		
	    	});
			
			function changeStatus(id, status, element){
				$.ajax({
	    			  type: "POST",
	    			  url: "/statusService",
	    			  data: {
	    				 id: id,
	    				 status: status
	    			  }
	    			})
	    			.done(function(msg){
						if (msg.sucesso && msg.sucesso == true){
							alert("Serviço alterado com sucesso");
							$(element).parent().parent().find(".status").text(getStatus(status));
							$(element).parent().find("button").remove();
						}else{
							alert("Erro ao alterar o serviço");
						}
	    			})
	    			.fail(function(jqXHR, textStatus, msg){
	    			     alert("Erro ao alterar o serviço");
	    			     console.log(msg);
	    			});
			}
			
			function getStatus(status){
				if (status == 0){
					return "Confirmado";
				}else{
					return "Rejeitado";
				}
			}
			
			</script>
	    <%}else if (listOfServices != null){%>
	    	<h2>Agenda vazia</h2>
	    <%}else{%>
	    	<h2>Erro ao obter os dados</h2>
	    <%}
    	
    }else{ %>
    	<h2>Erro ao obter os dados</h2>
    <%} %>
   </body>
</html>