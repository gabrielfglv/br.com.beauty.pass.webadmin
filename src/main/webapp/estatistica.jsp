<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.model.*,
		br.com.beauty.manager.StatisticsController,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="headImports.html"%>
    <title>Estatísticas da loja</title>
    <meta charset="utf-8"/>
  </head>
  <body>
  	<%
  		String idOnParameter = request.getParameter("storeId");
  		Statistic statistics = null;	
	  	
	  	if (idOnParameter != null){
	  		
	  		int id;
	  		try{
	  			id = Integer.parseInt(idOnParameter);
	  			statistics = StatisticsController.getInstance().getStatitics(id);	
	  		}catch(NumberFormatException e){
	  			System.out.print("Erro ao obter os dados do id para editar a loja");
	  		}
	  	}
  		
  	%>
  <a href="/listaDeLojas.jsp">voltar</a>
    <h1>Estatísticas da loja</h1>
    <br/>
    <% if (statistics != null){ 
    	
    %>
      ID: <input type="number" disabled value="<%=statistics.getStore().getId()%>">
    	<br />
      Título da loja: <input type="text" disabled name="title" maxlength="255" value="<%=statistics.getStore().getTitle()%>">
      <br />
      <table id="tableServices" style="width:100%" border="1">
		  <tr>
		  	<th >Acessos na loja</th>
		  	<th>Média de notas</th>
		  	<th>Serviços marcados</th>
		  	<th>Serviços não confirmados</th>
		  	<th>Serviços confirmados</th>
		  	<th>Serviços negados</th>
		  	<th alt="Confirmados e não confirmados">Total em serviços</th>
		  </tr>
		  <tr>
		  	<th>-</th>
		  	<th><%=statistics.getStore().getScore() %></th>
		  	<th><%=statistics.getQtAval() %></th>
		  	<%if (statistics.getQtAval() > 0){ %>
		  	<th><%=statistics.getQtNotConfirmed()%> (<%=(statistics.getQtNotConfirmed()*100)/statistics.getQtAval()%>%)</th>
		  	<th><%=statistics.getQtAcept() %> (<%=(statistics.getQtAcept()*100)/statistics.getQtAval()%>%)</th>
		  	<th><%=statistics.getQtDeclined() %> (<%=(statistics.getQtDeclined()*100)/statistics.getQtAval()%>%)</th>
		  	<%}else{ %>
		  	<th><%=statistics.getQtNotConfirmed()%></th>
		  	<th><%=statistics.getQtAcept() %> </th>
		  	<th><%=statistics.getQtDeclined() %> </th>
		  	<%} %>
		  	<th><%=(statistics.getTotalValue()+" R$").replace('.', ',')%></th> 
		  </tr>
		</table>
		<br/>
		<%
		if (statistics.getListOfServices() != null && !statistics.getListOfServices().isEmpty()){
		%>
		<h2>Serviços:</h2>
      <br />
      <table id="tableServices" style="width:100%" border="1">
		  <tr>
		  	<th >Id</th>
		  	<th>Título</th>
		  	<th>Acessos no serviço</th>
		  	<th>Média de notas</th>
		  	<th>Serviços marcados</th>
		  	<th>Total de ganhos</th>
		  	<th>Total de ganhos sob o total</th>
		  </tr>
		  <%
		  for (ServiceStatistic statisticService : statistics.getListOfServices()){
		  %>
		  <tr>
		  	<th><%=statisticService.getId() %></th>
		  	<th><%=statisticService.getTitle()%></th>
		  	<th>-</th>
		  	<th><%=statisticService.getAvgScore()%></th>
		  	<th><%=statisticService.getQtAval()%></th>
		  	<th><%=statisticService.getTotalValue()%></th>
		  	<th><%=((statisticService.getTotalValue()*100)/statistics.getTotalValue())%> %</th>
		  </tr>
		  <%
		  }
		  %>
		</table>
		<br/>
   		 <%
		}else{
   		 %>
    <br/> Loja não tem mais dados
    <%}
    
	}else{ %>
    	<h2>Erro ao obter a loja</h2>
    <%} %>
   </body>
</html>