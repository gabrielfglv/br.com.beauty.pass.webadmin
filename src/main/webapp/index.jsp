<!doctype html>
<%@page import="commons.StringUtils"%>
<%@page import="com.google.appengine.api.utils.SystemProperty"%>
<%@page import="br.com.beauty.manager.*"%>
<html>
  <head>
  	<%@ include file="headImports.html"%>
    <title>Beauty Pass - Admin</title>
    <meta charset="utf-8"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  </head>
  <body>
  		<h1>BEAUTY PASS - WEBSERVICE</h1>
		</br><a href="/listaDeLojas.jsp">Lista de lojas</a>
		</br><a href="/contato/lista.jsp">Contatos</a>
		</br><a href="/logs.jsp">Logs</a>
		</br><a href="documentacao/documentacao.jsp">Documentação</a>
		</br><a target="_blank" href="https://bitbucket.org/alanloiola/workspace/projects/BP">Repositório do código</a>
 		</br><a target="_blank" href="https://drive.google.com/open?id=16MEMFsN56i5go3WTjhAcg8PDK5tf3cbs">Download APK</a>
 
 		</br></br>
 		</br>Ambiente: <%=SystemProperty.environment.value()==null?"Prod":"Local" %>
 		</br>Hora servidor: <%=CalendarController.getInstance().getCalendarStr()%> 
  </body>
  
</html>
