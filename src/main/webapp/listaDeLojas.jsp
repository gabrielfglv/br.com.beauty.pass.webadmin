<!doctype html>
<%@page import="br.com.beauty.model.Store,
		br.com.beauty.manager.StoreController,
		java.util.ArrayList"%>
<html>
  <head>
  	<%@ include file="headImports.html"%>
    <title>Beauty Pass - Admin</title>
    <meta charset="utf-8"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  </head>
  <body>
  	<a href="/">voltar</a>
		<h1>LISTA DE LOJAS</h1>
	<a href="/cadastroLoja.jsp">Cadastrar Loja</a>	</br></br>
		<%
		
			StoreController controller = StoreController.getInstance();
			ArrayList<Store> listOfStores = controller.getListOfStores();
			
			
		 if (listOfStores == null){
		%>	
			<h2>Erro ao consultar o banco de dados</h2>
			<h3>Contate o adminsitrador</h3>
		<%
		}else if (listOfStores.size() == 0){
		%>
			<h2>Erro, n�o h� lojas cadastradas</h2>
		<%
		}else{
		%>
			<table style="width:100%" border="1">
			  <tr>
			    <th>ID</th>
			    <th>Nome</th>
			    <th>Endere�o</th>
			    <th>Valido</th>
			    <th>Categoria</th>
			    <th>CoordX</th>
			    <th>CoordY</th>
			    <th>Data de cria��o</th>
			    <th>Estat�sticas</th>
			    <th>Agenda</th>
			    <th>Editar</th>
			    <th>Servi�os</th>
			    <!-- <th>Apagar</th>-->
			  </tr>
			  <%
			  
			  for (Store store : listOfStores){
			  %>
			  <tr>
			    <td><%=store.getId()%></td>
			    <td><%=store.getTitle()%></td>
			    <td><%=store.getAdress()%></td>
			    <td><%=store.isValid()%></td>
			    <td><%=store.getCategory().getName()%></td>
			    <td><%=store.getCoordX()%></td>
			    <td><%=store.getCoordY()%></td>
			    <td><%=store.getCreateDate()%></td>
			    <th><a href="/estatistica.jsp?storeId=<%=store.getId()%>">Dados</a></th>
			    <th><a href="/agenda.jsp?storeId=<%=store.getId()%>">Agenda</a></th>
			    <th><a href="/editarLoja.jsp?storeId=<%=store.getId()%>">Editar</a></th>
			    <th><a href="/services/servicos.jsp?storeId=<%=store.getId()%>">Servi�os</a></th>
			    <!--  <th><a href="#" class="delete">Apagar</a></th>-->
			  </tr>
			  <%
			  }
			  %>
			</table>
			<script>
			$(".delete").click(function() {
	    		var id = $(this).parent().parent().find("td")[0].innerText;
	    		var title = $(this).parent().parent().find("td")[1].innerText;
	    		
	    		var confirmResponse = confirm("Voc� realmente deseja deletar '"+title+"' ?");
	    		if (confirmResponse == true) {
	    			deleteById(id);
	    		} 
	    		
	    		
	    	});
			
			function deleteById(id){
				$.ajax({
	    			  type: "POST",
	    			  url: "/removeStore",
	    			  data: {
	    				 idStore: id
	    			  }
	    			})
	    			.done(function(msg){
						if (msg.sucesso && msg.sucesso == true){
							alert("Loja deletada com sucesso!");
							window.location.reload();
						}else{
							alert("Erro ao deletar a loja");
						}
	    			})
	    			.fail(function(jqXHR, textStatus, msg){
	    			     alert("Erro ao deletar a loja");
	    			     console.log(msg);
	    			});
			}
			</script>
		<%
		}
		%>
  </body>
</html>
