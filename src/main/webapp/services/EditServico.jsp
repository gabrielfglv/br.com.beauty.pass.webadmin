<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.manager.*,
		br.com.beauty.model.*,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Edição de serviço</title>
    <meta charset="utf-8"/>
  </head>
  <body>
  	<%
  		String serviceId = request.getParameter("id");
  		
		ArrayList<Category> listOfTypesServices = ServiceController.getInstance().getTypeOfServices();
		int idService = -1;
		
		try{
			idService = Integer.parseInt(serviceId);
		}catch(Exception e){
			idService = -1;	
		}
		
	if (idService > -1 ){
  	
		Service service = ServiceController.getInstance().getService(idService);
		if (service != null){
		
		ArrayList<Category> listOfSegments = ServiceController.getInstance().getSegmentsStore(service.getIdStore());
		
	  	%>
 		<a href="/services/servicos.jsp?storeId=<%=service.getIdStore()%>">voltar</a>
	    <h1>Edição de serviço</h1>
	    <br/>
	    <form action="/editService" method="POST" onsubmit="return submitForm()">
	   	<input type="text" name="id" value="<%=service.getId()%>" style="display:none;"/>
      	<input type="text" name="storeId" value="<%=service.getIdStore()%>" style="display:none;"/>
	      id: <input type="text" value="<%=service.getId()%>" disabled/>
	      <br />
	      Título do serviço: <input type="text" required name="title" maxlength="255" value="<%=service.getTitle()%>">
	      <br />
	      Descrição: <textarea type="text" name="description"  value="<%=service.getDescription()%>"><%=service.getDescription()%></textarea>
	      <br />
	      Ordem: <input type="number" required name="ordem" value="<%=service.getPosition()%>" />
	      <br />
	      Ativo: <input type="checkbox"  name="ativo" <%=service.isValid()?"checked":""%>/>
	      <br />
	      Destaque: <input type="checkbox"  name="destaque" <%=service.isHightlight()?"checked":""%>/>
	      <br />
	       Duração do serviço: <input type="time" required name="duration" value="<%=service.getDurationStr()%>"/>
	      <br /> 
	       Horarios disponiveis: <input type="time" required name="inicioHora" value="<%=service.getTimeStartServiceStr()%>"/> 
	       até <input type="time" required name="fimHora" value="<%=service.getTimeEndServiceStr()%>"/>
	      <br /> 
	      Disponivel nos dias da semana: 
			<input type="checkbox" name="day0" <%=service.hasDayOfWeek(0)?"checked":""%>/>Domingo
			<input type="checkbox" name="day1" <%=service.hasDayOfWeek(1)?"checked":""%>/>Segunda-feira
			<input type="checkbox" name="day2" <%=service.hasDayOfWeek(2)?"checked":""%>/>Terça-feira
			<input type="checkbox" name="day3" <%=service.hasDayOfWeek(3)?"checked":""%>/>Quarta-feira
			<input type="checkbox" name="day4" <%=service.hasDayOfWeek(4)?"checked":""%>/>Quinta-feira
			<input type="checkbox" name="day5" <%=service.hasDayOfWeek(5)?"checked":""%>/>Sexta-feira
			<input type="checkbox" name="day6" <%=service.hasDayOfWeek(6)?"checked":""%>/>Sabado
	      <br />
	      Vagar por horário: <input type="number" value="<%=service.getVacanciesByTime()%>" required name="limite" />
	      <br />
	      Tipo de serviço:
	      <br /> 
	      <select name="typeService" required>
			<%
			if (listOfTypesServices != null){
				int categoryService = service.getTypeService().getId();
				for (Category category : listOfTypesServices){
			%>
				<option value="<%=category.getId()%>" <%=category.getId()==categoryService?"selected":""%>><%=category.getName()%></option>
				<%
				}
			}
			%>
			</select>
		<br />
	      Segmento: 
	      <select name="segment">
	      		<option value=""></option>
	      		<%
	      		if (listOfSegments != null){
	      			int segmentService = service.getSegment()==null?-1:service.getSegment().getId();
	      			for (Category category : listOfSegments){
	      		%>
	      			<option value="<%=category.getId()%>" <%=category.getId()==segmentService?"selected":""%>><%=category.getName()%></option> 
	      		<%
	      			}
	      		}
	      		%>
	      	</select>
			<br />
	      Opções multiplas: <input type="checkbox" name="muilti" <%=service.isMultiOption()?"checked":""%>/>
	      <br />
	       <br />
	      Lista de serviços:
	      <input id="optionsToAdd" name="optionsToAdd" type="hidden" />
	      <input id="optionsToEdit" name="optionsToEdit" type="hidden" />
	      <input id="optionsToDelete" name="optionsToDelete" type="hidden" />
	      <br />
	      <button id="add" onclick="return false;">Adicionar uma nova opção do serviço</button>
	     <button id="revert" onclick="return false;">Reverter linha deletada</button>
      	</br>
	      <table id="tableServices" style="width:100%" border="1">
				  <tr>
				  	<th style="display:none">id</th>
				  	<th>Título</th>
				  	<th>Descrição</th>
				  	<th>Valor</th>
				  	<th>Position</th>
				  	<th>Ativo</th>
				  	<th>Remover</th>
				  </tr>
				  <%
				  for (ServiceOption option : service.getServiceOption()){
					%>
					<tr>
					  	<th style="display:none"><input class="id" value="<%=option.getId()%>"/></th>
					  	<th><input type="text" required value="<%=option.getTitle()%>"/></th>
					  	<th><textarea type="text"><%=option.getDescription()==null?"":option.getDescription()%></textarea></th>
					  	<th><input type="text" required value="<%=option.getValor()%>"/></th>
					  	<th><input type="number" required value="<%=option.getOrdem()%>"/></th>
					  	<th><input type="checkbox" <%=option.isValid()?"checked":""%>/></th>
					  	<th><button onclick="removeRow(this); return false;" class="remove_button">Remover</button></th>
					</tr>
					<% 
				  }
				  %>
			</table>
	      <input type="submit"  value="Submit" />
	    </form>
	    <script>
		
		   	var jsonServicesAdd = [];
		   	var jsonServicesEdit = [];
	    	var rowsRemoved = [];
	    	var servicesIdsToDelete = [];
	    	
	    	$("#add").click(function() {
	    		var htmlRow = '<tr>';
	    		htmlRow += '<th style="display:none"><input class="id" type="number" value="-1" required/></th>'
		    	htmlRow += '<th><input type="text" required/></th>'
	    		htmlRow += '<th><textarea type="text"></textarea></th>'
	 			htmlRow += '<th><input type="text" required/></th>'
		  		htmlRow += '<th><input type="number" value="1" required/></th>'
		  	  	htmlRow += '<th><input type="checkbox" checked/></th>'
				htmlRow += '<th><button onclick="removeRow(this);" class="remove_button">Remover</button></th>'
					    		
	    		$("#tableServices").find("tbody").append(htmlRow+"</tr>");
	    	});
	    	

        	//Remove a linha 
        	function removeRow(button){
        		var rowToRemove = $(button).parent().parent();
        		var idRow = $($(rowToRemove).find("th")[0]).find('input').val();
        		
        		if (idRow != -1)
        			servicesIdsToDelete.push(parseInt(idRow));
        		
        		rowsRemoved.push(rowToRemove);
        		rowToRemove.remove();
        		
        		refreshValuesServices();
        		return false;
        	}
        	
        	//Reverte a linha deletada
    		$("#revert").click(function() {
        		var row = rowsRemoved.pop();
        		if (row){
        			var id = $(row).find("input").val();
        			servicesIdsToDelete.pop(parseInt(id));
        			$("#tableServices").find("tbody").append(row);
        		}
        		refreshValuesServices();
        	});
	        	
	    	//Atualiza os valores pro JSON
	    	function refreshValuesServices(){
	    		var rows = $("#tableServices").find("tbody").find("tr");
	    		
	    		for (var row in rows){
	    			
	    			if (row > 0){
		    			var objRow = getObjectRow(rows[row]);
		    			
						if (objRow != null){
							if (objRow.id == -1){
			    				jsonServicesAdd.push(objRow);
							}else{
								jsonServicesEdit.push(objRow);
							}
	    				}
	    			}
	    		}
	    		
	   			$("#optionsToEdit").val(JSON.stringify(jsonServicesEdit));
	   			$("#optionsToAdd").val(JSON.stringify(jsonServicesAdd));
	   			$("#optionsToDelete").val(servicesIdsToDelete);
	    	}
	    	
	    	//Obtem o objeto de acordo com as linhas
	    	function getObjectRow(row){
	    		
	    		var obj = {};
	    		var colunas = $(row).find("th");
	    		
	    		var id = $(colunas[0]).find("input").val();
				var name = $(colunas[1]).find("input").val();
				var description = $(colunas[2]).find("textarea").val();
				var valor = $(colunas[3]).find("input").val();
				var position = $(colunas[4]).find("input").val();
				var valid = $(colunas[5]).find("input").prop("checked");
			
				if (id && name && valor && position){
					obj.id = id;
					obj.name = name;
					obj.valor = parseFloat(valor.replace(',','.'));
					obj.valid = valid;
					obj.position = parseInt(position);
					obj.description = description?description:null;
	    			return obj;
				}else{
					return null;
				}
	    	}
	    	
	    	function submitForm(){
	    		refreshValuesServices();
    			if ($("#tableServices").find("input:checked").length > 0){
    				return true;	
    			}else{
    				alert("Pelo menos uma opção ativa");
        			return false;
    			}
	    	}
	    </script>
    <%}else{%>
		<h1>ERRO AO OBTER O SERVIÇO</h1>
	<%}
    }else{ %>
    	<h1>ERRO AO OBTER O SERVIÇO</h1>
    <%} %>
   </body>
</html>