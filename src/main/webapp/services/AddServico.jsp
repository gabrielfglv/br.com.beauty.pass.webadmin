<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%@page import="br.com.beauty.model.Category,
		br.com.beauty.manager.*,
		java.util.ArrayList"%>
  <head>
  	<%@ include file="../headImports.html"%>
    <title>Cadastro de novo serviço</title>
    <meta charset="utf-8"/>
  </head>
  <body>
  	<%
  		String storeId = request.getParameter("idStore");
		ArrayList<Category> listOfTypesServices = ServiceController.getInstance().getTypeOfServices();
		int idStore = Integer.parseInt(storeId);
  		ArrayList<Category> listOfSegments = ServiceController.getInstance().getSegmentsStore(idStore);
  	%>
  <a href="/services/servicos.jsp?storeId=<%=storeId%>">voltar</a>
    <h1>Cadastro de novo serviço</h1>
    <br/>
    <form action="/addService" method="POST" onsubmit="return submitForm()">
      <input type="text" name="storeId" value="<%=storeId%>" style="display:none;"/>
      Título do serviço: <input type="text" required name="title" maxlength="255">
      <br />
      Descrição: <textarea type="text" name="description"></textarea>
      <br />
      Ordem: <input type="number" required name="ordem" value="1" />
      <br />
      Ativo: <input type="checkbox"  name="ativo" checked />
      <br />
      Destaque: <input type="checkbox"  name="destaque" />
      <br />
       Duração do serviço: <input type="time" required name="duration" />
      <br /> 
       Horarios disponiveis: <input type="time" required name="inicioHora" /> até <input type="time" required name="fimHora" />
      <br /> 
      Disponivel nos dias da semana: 
		<input type="checkbox" name="day0"/>Domingo
		<input type="checkbox" name="day1"/>Segunda-feira
		<input type="checkbox" name="day2"/>Terça-feira
		<input type="checkbox" name="day3"/>Quarta-feira
		<input type="checkbox" name="day4"/>Quinta-feira
		<input type="checkbox" name="day5"/>Sexta-feira
		<input type="checkbox" name="day6"/>Sabado
      <br />
      Vagar por horário: <input type="number" value="1" required name="limite" />
      <br />
      Tipo de serviço:
      <br /> 
      <select name="typeService" required>
		<%
		if (listOfTypesServices != null){
			for (Category category : listOfTypesServices){
		%>
			<option value="<%=category.getId()%>"><%=category.getName()%></option>
			<%
			}
		}
		%>
		</select>
	<br />
      Segmento: 
      <select name="segment">
      		<option value=""></option>
      		<%
      		if (listOfSegments != null){
      			for (Category category : listOfSegments){
      		%>
      			<option value="<%=category.getId()%>"><%=category.getName()%></option> 
      		<%
      			}
      		}
      		%>
      	</select>
		<br />
      Opções multiplas: <input type="checkbox" name="muilti" />
      <br />
       <br />
      Lista de serviços:
      <input id="optionsToAdd" name="optionsToAdd" type="hidden" />
      <br />
      <button id="add" onclick="return false;">Adicionar uma nova opção do serviço</button>
     </br>
      <table id="tableServices" style="width:100%" border="1">
			  <tr>
			  	<th style="display:none">id</th>
			  	<th>Título</th>
			  	<th>Descrição</th>
			  	<th>Valor</th>
			  	<th>Position</th>
			  	<th>Ativo</th>
			  	<th>Remover</th>
			  </tr>
		</table>
      <input type="submit"  value="Submit" />
    </form>
    <script>
    
    	var jsonServicesAdd = [];
    	
    	$("#add").click(function() {
    		var htmlRow = '<tr>';
    		htmlRow += '<th><input type="text" required/></th>'
    		htmlRow += '<th><textarea type="text"></textarea></th>'
 			htmlRow += '<th><input type="text" required/></th>'
	  		htmlRow += '<th><input type="number" value="1" required/></th>'
	  	  	htmlRow += '<th><input type="checkbox" checked/></th>'
			htmlRow += '<th><button onclick="removeRow(this);" class="remove_button">Remover</button></th>'
				    		
    		$("#tableServices").find("tbody").append(htmlRow+"</tr>");
    	});
    	
    	//Remove a linha 
    	function removeRow(button){
    		var rowToRemove = $(button).parent().parent();
    		rowToRemove.remove();
    		refreshValuesServices();
    		return false;
    	}
    	
    	//Atualiza os valores pro JSON
    	function refreshValuesServices(){
    		var rows = $("#tableServices").find("tbody").find("tr");
    		
    		jsonServicesAdd = [];
    		
    		for (var row in rows){
    			
    			if (row > 0){
	    			var objRow = getObjectRow(rows[row]);
	    			
					if (objRow != null){
		    			jsonServicesAdd.push(objRow);
    				}
    			}
    		}
    		
   			$("#optionsToAdd").val(JSON.stringify(jsonServicesAdd));
    	}
    	
    	//Obtem o objeto de acordo com as linhas
    	function getObjectRow(row){
    		
    		var obj = {};
    		var colunas = $(row).find("th");
    		
			var name = $(colunas[0]).find("input").val();
			var description = $(colunas[1]).find("textarea").val();
			var valor = $(colunas[2]).find("input").val();
			var position = $(colunas[3]).find("input").val();
			var valid = $(colunas[4]).find("input").prop("checked");
		
			if (name && valor && position){
				obj.id = -1;
				obj.name = name;
				obj.valor = parseFloat(valor.replace(',','.'));
				obj.valid = valid;
				obj.position = parseInt(position);
				obj.description = description?description:null;
    			return obj;
			}else{
				return null;
			}
    	}
    	
    	function submitForm(){
    		refreshValuesServices();
    		if (jsonServicesAdd.length > 0){
    			if ($("#tableServices").find("input:checked").length > 0){
    				return true;	
    			}else{
    				alert("Pelo menos uma opção ativa");
        			return false;
    			}
    		}else{
    			alert("Crie ao menos uma opção");
    			return false;
    		}
    	}
    </script>
   </body>
</html>